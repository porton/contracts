
## Table of Contents

- [Explanation](#explanation)
- [Install](#install)
- [Testing](#testing)
- [Testnets](#testnets)
- [Contribute](#contribute)
- [License](#license)

## Explanation
This is a Non-Fungible-Token (NFT) conforming to ERC721. Each token issued by this contract represents an identity that can vote in the prediction markets (different repository). The contract will mint and transfer an identity token to the user who calls createMyIdentity after having approved this contract to burn a number of FVT tokens. We call this amount of burned tokens the price. The price follows the following schedule. 
- Every purchase multiplies the price by identityIncreaseFactor / indentityIncreaseDenominator
- Every block that goes by without a purchase decreases the price by identityDecayFactor
- The price cannot drop below identityPriceFloor


## Testnets
The official ropsten addresses are:
- SafeMathLib: [0x2365A147D3fd5522e375e1c17eeC780095b73d49](https://ropsten.etherscan.io/address/0x2365A147D3fd5522e375e1c17eeC780095b73d49)
- Token: [0x3a7e9F41C1d28F87Fab37D557529e8c4e6e4a9F0](https://ropsten.etherscan.io/token/0x3a7e9F41C1d28F87Fab37D557529e8c4e6e4a9F0)
- BondingCurve: [0x2f0dAe36Fe1b00AcaC4A73bBb1871648a4053CC6](https://ropsten.etherscan.io/address/0x2f0dAe36Fe1b00AcaC4A73bBb1871648a4053CC6)


## Install 

``` bash
# install dependencies
yarn
```

## Testing
``` bash
# deploy to local blockchain
truffle deploy
# run tests 
truffle test
```

## Contribute

To report bugs within this package, create an issue in this repository.
For security issues, please contact chris@finance.vote
When submitting code ensure that it is free of lint errors and has 100% test coverage.


## License

[GNU General Public License v3.0 (c) 2018 Fragments, Inc.](./LICENSE)

