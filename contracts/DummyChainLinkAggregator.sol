// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.7.4;

import "./interfaces/AggregatorV3Interface.sol";

contract DummyChainLinkAggregator is AggregatorV3Interface {

    int priceResponse = 0;

    constructor(int price) {
        priceResponse = price;
    }

    function decimals() external view override returns (uint8) {
        return 18;
    }

    function description() external view override returns (string memory) {
        return "wow great description";
    }

    function version() external view override returns (uint256) {
        return 1;
    }

    function setPrice(int newPrice) public {
        priceResponse = newPrice;
    }

    function getRoundData(uint80 _roundId)
        public
        view
        override
        returns (
          uint80 roundId,
          int256 answer,
          uint256 startedAt,
          uint256 updatedAt,
          uint80 answeredInRound
        ){
        return (0, 0, 0, 0, 0);
    }

    function latestRoundData()
        public
        view
        override
        returns (
          uint80 roundId,
          int256 answer,
          uint256 startedAt,
          uint256 updatedAt,
          uint80 answeredInRound
        ){
        return (0, priceResponse, 0, 0, 0);
    }


}
