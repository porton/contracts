// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.7.4;

import "./interfaces/AggregatorV3Interface.sol";

contract DummyChainLinkAggregatorETH is AggregatorV3Interface {

    AggregatorV3Interface public feedTokenETH;
    AggregatorV3Interface public feedETHUSD;


    constructor(address feedA, address feedB) {
        feedTokenETH = AggregatorV3Interface(feedA);
        feedETHUSD = AggregatorV3Interface(feedB);
    }

    function decimals() external view override returns (uint8) {
        // sum because we multiply the feeds, adding the exponents
        return feedETHUSD.decimals() + feedTokenETH.decimals();
    }

    function description() external view override returns (string memory) {
        return "UMA/USD";
    }

    function version() external view override returns (uint256) {
        return 1;
    }

    function getRoundData(uint80 _roundId)
        public
        view
        override
        returns (
          uint80 roundId,
          int256 answer,
          uint256 startedAt,
          uint256 updatedAt,
          uint80 answeredInRound
        ){
        return (0, 0, 0, 0, 0);
    }

    function latestRoundData()
        public
        view
        override
        returns (
          uint80 roundId,
          int256 answer,
          uint256 startedAt,
          uint256 updatedAt,
          uint80 answeredInRound
        ){
        (, int priceTokenETH, , ,) = feedTokenETH.latestRoundData();
        (, int priceETHUSD, , ,) = feedETHUSD.latestRoundData();
        return (0, priceTokenETH * priceETHUSD, 0, 0, 0);
    }


}
