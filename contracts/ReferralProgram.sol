// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.7.4;

import './interfaces/IERC721.sol';

contract ReferralProgram {

    IERC721 public identityToken;

    // mapping from referee to referer
    mapping (uint => uint) public referralMap;

    event ReferralMade(address indexed owner, uint indexed referer, uint indexed referee);

    constructor(address identityAddress) {
        identityToken = IERC721(identityAddress);
    }

    function setReferral(uint referer, uint referee) public {
        require(identityToken.ownerOf(referee) == msg.sender, 'Cannot claim referral for token you do not own');
        require(referralMap[referee] == 0, 'Already set referral');
        referralMap[referee] = referer;
        emit ReferralMade(msg.sender, referer, referee);
    }


}
