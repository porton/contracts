// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.7.4;

interface ITrollbox {
    function withdrawWinnings(uint voterId) external;
    function updateAccount(uint voterId, uint tournamentId, uint roundId) external;
    function isSynced(uint voterId, uint tournamentId, uint roundId) external view returns (bool);
    function roundAlreadyResolved(uint tournamentId, uint roundId) external view returns (bool);
    function resolveRound(uint tournamentId, uint roundId, uint winningOption) external;
    function getCurrentRoundId(uint tournamentId) external returns (uint);
}
