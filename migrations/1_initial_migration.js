const SafeMathLib = artifacts.require("SafeMathLib");
const Identity = artifacts.require("Identity");
const Token = artifacts.require("Token")
    , Auction = artifacts.require('Auction')
    , LiquidityMining = artifacts.require('LiquidityMining')
    , LPToken = artifacts.require('DummyLPToken')
    , Trollbox = artifacts.require('Trollbox')
    , TrollboxProxy = artifacts.require('TrollboxProxy')
    , Vault = artifacts.require('Vault')
    , DummyUniswapRouter = artifacts.require('DummyUniswapRouter')
    , DummyChainLinkAggregator = artifacts.require('DummyChainLinkAggregator')
    , ChainLinkOracle = artifacts.require('ChainLinkOracle')
    , numTokens = '1000000000'
    , tokensForSale = web3.utils.toWei('20000000')
    , initialPrice = web3.utils.toWei('0.000025') // approximately 0.01 USD
    , initialTokens = web3.utils.toWei('500000') // half a million tokens
    , initialDecay = web3.utils.toWei('0.00000001') // 1% of price floor, 1/2500 of starting price
    , minimumPrice = web3.utils.toWei('0.000001') // 4% of starting price

let uniswapAddress = '', safeAddress = '', startBlock = 0, mgmt = '', gasPrice = ''

let tokenAddress
  , destinations = []
  , tokenAllocations = []
  , lockPeriods = []
  , vestingPeriodEnds = []
  , startBlocks = []

async function deployVault (deployer, network, accounts) {
    if (network === 'ropsten') {
        // FVT
        tokenAddress = '0x61dc5c017591ed096dd436fb2812f5954be8cd48'
        destinations = ['0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1']
        tokenAllocations = [web3.utils.toWei(numTokens)]
        lockPeriods = [4 * 60]
        vestingPeriodEnds = [4 * 60]
        startBlocks = [8926913 + (4 * 10)]
    } else if (network === 'development') {
        await deployer.deploy(SafeMathLib)
        token = await Token.deployed();
        tokenAddress = token.address;
        destinations = ['0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1', '0xFFcf8FDEE72ac11b5c542428B35EEF5769C409f0']
        tokenAllocations = [web3.utils.toWei('10000000'), web3.utils.toWei('10000000')]
        lockPeriods = [26, 29]
        vestingPeriodEnds = [25, 26]
        startBlocks = [10, 12]
    } else if (network === 'mainnet') {

    }
    await deployer.link(SafeMathLib, Vault)
    await deployer.deploy(Vault, tokenAddress, destinations, tokenAllocations, lockPeriods, vestingPeriodEnds, startBlocks);
}

async function deploySafeMath(deployer, network, accounts) {
    gasPrice = '60000000000'
    await deployer.deploy(SafeMathLib, {from: accounts[0], gasPrice: gasPrice})
}

async function deployIdentity(deployer, network, accounts) {
  if (network === 'development') {
    mgmt = accounts[0];
  } else if (network === 'ropsten') {
    mgmt = accounts[0];
  } else if (network === 'main') {
    mgmt = '0x288fE43139741F91a8Cbb6F4adD83811c794851b'
    gasPrice = '60000000000' // 60gwei
  }
  await deployer.link(SafeMathLib, Identity);
  await deployer.deploy(Identity, mgmt, {from: accounts[0], gasPrice: gasPrice})
}

async function connectContracts() {
  const identity = await Identity.deployed()
  const token = await Token.deployed()
  const trollBox = await Trollbox.deployed()
  await identity.setToken(token.address)
  await trollBox.setToken(token.address)
}

async function deployAuction (deployer, network, accounts) {
  console.log('network', network)
  console.log('accounts', accounts)

  if (network === 'development') {
    mgmt = accounts[0]
    await deployer.deploy(DummyUniswapRouter)
    const uniswap = await DummyUniswapRouter.deployed()

    startBlock = 0
    tokenAddress = (await Token.deployed()).address
    uniswapAddress = uniswap.address
    safeAddress = accounts[1]
  } else if (network === 'ropsten') {
    mgmt = '0x288fE43139741F91a8Cbb6F4adD83811c794851b'
    startBlock = 8997620
    tokenAddress = '0xF7eF90B602F1332d0c12cd8Da6cd25130c768929'
    uniswapAddress = '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'
    safeAddress = '0x96FFB474BCFE44235082c07C7f3BE07cb9fbC08f'
  } else if (network === 'main') {
    mgmt = ''
    tokenAddress = ''
    startBlock = 0
    uniswapAddress = '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'
    safeAddress = ''
  }
  await deployer.link(SafeMathLib, Auction)
  await deployer.deploy(
    Auction,
    mgmt,
    tokenAddress,
    uniswapAddress,
    startBlock,
    tokensForSale,
    initialPrice,
    initialTokens,
    initialDecay,
    minimumPrice,
    safeAddress
  )
  const auction = await Auction.deployed()

  if (network !== 'main') {
    const fvt = await Token.at(tokenAddress)
    await fvt.transfer(auction.address, tokensForSale)
  }
}

async function deployToken(deployer, network, accounts) {
    let bank;
    if (network === 'ropsten') {
        bank = '0x288fE43139741F91a8Cbb6F4adD83811c794851b'
    } else if (network === 'development') {
        await deployer.deploy(SafeMathLib);
        bank = accounts[0]
    } else if (network === 'main') {

    }
    await deployer.link(SafeMathLib, Token);
    await deployer.deploy(Token, bank);
}

async function deployTrollbox (deployer, network, accounts) {
  if (network === 'development') {
    mgmt = accounts[0]
    rankManager = accounts[1]
    gasPrice = '1000000000'
  } else if (network === 'ropsten') {
    mgmt = accounts[0]
    rankManager = accounts[0]
    gasPrice = '1000000000'
  } else if (network === 'main') {
    mgmt = '0x288fE43139741F91a8Cbb6F4adD83811c794851b'
    rankManager = mgmt
    gasPrice = '60000000000' // 60 gwei
  }
  const i = await Identity.deployed()
  identityAddress = i.address
  await deployer.link(SafeMathLib, Trollbox)
  await deployer.deploy(Trollbox, mgmt, rankManager, identityAddress, {from: accounts[0], gasPrice: gasPrice})
  const trollbox = await Trollbox.deployed()
  await deployer.deploy(TrollboxProxy, trollbox.address)
}

async function deployLiquidityMining(deployer, network, accounts) {
  mgmt = accounts[0]
  const pulseLengthBlocks = 10000
  const pulseAmplitude = web3.utils.toWei('1000')
  const maxStakers = 10
  await deployer.link(SafeMathLib, LPToken);
  await deployer.deploy(LPToken, mgmt)
  const baseToken = await Token.deployed()
  const lpToken = await LPToken.deployed()
  await deployer.link(SafeMathLib, LiquidityMining)
  await deployer.deploy(LiquidityMining, baseToken.address, lpToken.address, mgmt, pulseLengthBlocks, pulseAmplitude, maxStakers)
}

async function fundContracts (deployer, network, accounts) {
  mgmt = accounts[0]
  const token = await Token.deployed()
  const auction = await Auction.deployed()
  const vault = await Vault.deployed()
  const trollBox = await Trollbox.deployed()
  const liquidityMining = await LiquidityMining.deployed()
  await token.transfer(auction.address, web3.utils.toWei('20000000'), {from: mgmt})
  await token.transfer(vault.address, web3.utils.toWei('20000000'), {from: mgmt})
  await token.transfer(trollBox.address, web3.utils.toWei('20000000'), {from: mgmt})
  await token.transfer(liquidityMining.address, web3.utils.toWei('20000000'), {from: mgmt})
}

async function deployChainLink (deployer, network, accounts) {
  mgmt = accounts[0]
  const trollbox = await Trollbox.deployed()
  const token = await Token.deployed()
  const defaultTickerSymbols = ['LINK', 'WBTC', 'DAI', 'UNI', 'MKR', 'COMP', 'UMA', 'YFI', 'SNX', 'REN'].map(x => web3.utils.asciiToHex(x));
  const initialPrices = ['1',"2",'3','4','5','6','7','8','9','10'].map(x => web3.utils.toWei(x))
  console.log({initialPrices, defaultTickerSymbols})
  await deployer.deploy(ChainLinkOracle, mgmt, trollbox.address, token.address, 1, defaultTickerSymbols, initialPrices)
  const oracle = await ChainLinkOracle.deployed()
  const aggs = {}
  for (var i = 0; i < defaultTickerSymbols.length; i++) {
    const price = web3.utils.toWei(Math.floor(Math.random() * 100).toString())
    const rcpt = await deployer.deploy(DummyChainLinkAggregator, price)
    const symbol = defaultTickerSymbols[i]
    aggs[web3.utils.hexToString(symbol)] = rcpt.contract._address
    await oracle.addFeed(symbol, rcpt.contract._address)
  }
  console.log(aggs)
}

module.exports = function(deployer, network, accounts) {
  deployer.then(async () => {
    await deploySafeMath(deployer, network, accounts)
    await deployIdentity(deployer, network, accounts);
    await deployToken(deployer, network, accounts);
    await deployTrollbox(deployer, network, accounts);
    await connectContracts()
    await deployVault(deployer, network, accounts)
    await deployAuction(deployer, network, accounts)
    await deployLiquidityMining(deployer, network, accounts)
    await fundContracts(deployer, network, accounts)
    await deployChainLink(deployer, network, accounts)
  });
};
