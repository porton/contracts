/* global artifacts, web3 */

const Trollbox = artifacts.require('Trollbox')
    , TrollboxProxy = artifacts.require('TrollboxProxy')
    , LiquidityMining = artifacts.require('LiquidityMining')
    , ChainLinkOracle = artifacts.require('ChainLinkOracle')
    , LPToken = artifacts.require('DummyLPToken')
    , Identity = artifacts.require('Identity')
    , tournamentHash = '0x2bffe98db7ae36ac27dd176b5b4f1ddf1d319d2084570230ed75dfdca89030ea'
    , voteHash = tournamentHash
    , startTime = 0
    , tournamentId = 1
    , roundLengthSeconds = 30
    , tokenRoundBonus = web3.utils.toWei('100')
    , tokenListENS = web3.utils.stringToHex('defi.cmc.eth')
    , minRank = 0
    , voiceUBI = 100

async function mineOneBlock () {
  await web3.currentProvider.send({
    jsonrpc: '2.0',
    method: 'evm_mine',
    id: new Date().getTime()
  }, () => {})
}

async function increaseTime(bySeconds) {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_increaseTime',
        params: [bySeconds],
        id: new Date().getTime(),
    }, (err, result) => {
      if (err) { console.error(err) }

    });
    await mineOneBlock();
}


async function generateEvents (deployer, network, accounts) {
  if (network === 'development') {
    const trollbox = await Trollbox.deployed()
    const proxy = await TrollboxProxy.deployed()
    const oracle = await ChainLinkOracle.deployed()
    const challengePeriod = 10
    await trollbox.createTournament(tournamentHash, startTime, roundLengthSeconds, tokenRoundBonus, tokenListENS, oracle.address, minRank, voiceUBI)
    await oracle.setChallengePeriod(challengePeriod, {from: accounts[0]})

    const identity = await Identity.deployed()
    await identity.createIdentityFor(accounts[0], { from: accounts[0] })
    await identity.createIdentityFor(accounts[1], { from: accounts[0] })
    await identity.createIdentityFor(accounts[2], { from: accounts[0] })
    await identity.createIdentityFor(accounts[3], { from: accounts[0] })
    await identity.createIdentityFor(accounts[4], { from: accounts[0] })
    const roundId = await trollbox.getCurrentRoundId(1)
        , t = await trollbox.tournaments(1)
    await trollbox.vote(2, tournamentId, [ 1, 2, 3 ], [ 1, 2, 3 ], voteHash, 0, { from: accounts[1] })
    await trollbox.vote(3, tournamentId, [ 1, 2, 3 ], [ 1, 2, 3 ], voteHash, 0, { from: accounts[2] })
    await trollbox.vote(4, tournamentId, [ 1, 2, 3 ], [ 1, 2, 3 ], voteHash, 0, { from: accounts[3] })
    await trollbox.vote(5, tournamentId, [ 1, 2 ], [ 4, 8 ], voteHash, 0, { from: accounts[4] })
    await increaseTime(roundLengthSeconds + 10)
    await trollbox.vote(2, tournamentId, [ 1, 2, 3 ], [ 3, 2, 3 ], voteHash, 0, { from: accounts[1] })
    await trollbox.vote(3, tournamentId, [ 1, 2, 3 ], [ 3, 2, 3 ], voteHash, 0, { from: accounts[2] })
    await trollbox.vote(4, tournamentId, [ 1, 2, 3 ], [ 3, 2, 3 ], voteHash, 0, { from: accounts[3] })
    await trollbox.vote(5, tournamentId, [ 1, 6 ], [ 4, 8 ], voteHash, 0, { from: accounts[4] })
    await increaseTime(roundLengthSeconds + 10)

//    await trollbox.resolveRound(tournamentId, 1, 2, { from: accounts[0] })
    await oracle.proposeWinner(1, { from: accounts[0] })
    await increaseTime(challengePeriod + 1)
    await oracle.confirmWinnerUnchallenged(1, { from: accounts[0] })

    for (let i = 0; i < 15; i++) {
      const roundId = 2 + i
      await trollbox.vote(5, tournamentId, [ 1, 6 ], [ 4, 8 ], voteHash, 0, { from: accounts[4] })
      await increaseTime(roundLengthSeconds + 10)
      await oracle.proposeWinner(roundId, { from: accounts[0] })
      await increaseTime(challengePeriod + 1)
      await oracle.confirmWinnerUnchallenged(roundId, { from: accounts[0] })
    }
  } else if (network === 'ropsten') {
    const trollbox = await Trollbox.deployed()
    await trollbox.createTournament(tournamentHash, startTime, roundLengthSeconds, tokenRoundBonus, tokenListENS, accounts[0], minRank, voiceUBI)
  }
}

async function generateEventsLiquidity (deployer, network, accounts) {
  const lpToken = await LPToken.deployed()
  const liquidityMining = await LiquidityMining.deployed()

  async function claimSlot(slotId, burnRate, deposit, fromAddr) {
    await lpToken.approve(liquidityMining.address, web3.utils.toWei(deposit), {from: fromAddr})
    await liquidityMining.claimSlot(slotId, web3.utils.toWei(burnRate), web3.utils.toWei(deposit), {from: fromAddr})
  }

  await lpToken.transfer(accounts[1], web3.utils.toWei('10000000'), {from: accounts[0]})
  await lpToken.transfer(accounts[2], web3.utils.toWei('10000000'), {from: accounts[0]})
  await lpToken.transfer(accounts[3], web3.utils.toWei('10000000'), {from: accounts[0]})
  await lpToken.transfer(accounts[4], web3.utils.toWei('10000000'), {from: accounts[0]})
  await lpToken.transfer(accounts[5], web3.utils.toWei('10000000'), {from: accounts[0]})
  await lpToken.transfer(accounts[6], web3.utils.toWei('10000000'), {from: accounts[0]})

  await claimSlot(1, '1', '10', accounts[1])
  await claimSlot(2, '2', '20', accounts[2])
  await claimSlot(3, '3', '100', accounts[3])
  await claimSlot(4, '5', '50', accounts[4])
  await claimSlot(5, '10', '1000', accounts[5])
  await claimSlot(6, '1', '10', accounts[6])

}

module.exports = (deployer, network, accounts) => {
  deployer.then(async () => {
    await generateEvents(deployer, network, accounts)
    await generateEventsLiquidity(deployer, network, accounts)
  })
}
