/* global artifacts, web3, afterEach, beforeEach, it, contract */

const Web3 = require('web3')
const web3 = new Web3('ws://localhost:8545')
    , bN = web3.utils.toBN
    , l = console.log
    , Auction = artifacts.require('Auction')
    , Token = artifacts.require('Token')
    , DummyUniswapRouter = artifacts.require('DummyUniswapRouter')
    , SafeMathLib = artifacts.require('SafeMathLib')

const {
  BN, // Big Number support
  constants, // Common constants, like the zero address and largest integers
  expectEvent, // Assertions for emitted events
  expectRevert // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers')

const { ZERO_BYTES32, ZERO_ADDRESS } = constants // empty hash

const chai = require('chai')
    , bnChai = require('bn-chai')
chai.use(bnChai(BN))
chai.config.includeStack = true
const expect = chai.expect

async function mineOneBlock () {
  await web3.currentProvider.send({
    jsonrpc: '2.0',
    method: 'evm_mine',
    id: new Date().getTime()
  }, () => {})
}

async function mineBlocks (numBlocks) {
  for (var i = 0; i < numBlocks; i++) {
    await mineOneBlock()
  }
}

async function getChainHeight () {
  const tip = await web3.eth.getBlock('latest')
  return new BN(tip.number)
}

contract('Auction', function (accounts) {
  const state = {}
      , decimals = new BN('1000000000000000000')

  beforeEach(async () => {
    const safeMath = await SafeMathLib.new()
    await Token.link('SafeMathLib', safeMath.address)
    await Auction.link('SafeMathLib', safeMath.address)

    state.mgmt = accounts[0]
    state.initialBalance = new BN(1e9)
    state.token = await Token.new(state.mgmt)
    state.uniswap = await DummyUniswapRouter.new()
    state.gasLimit = 170000
    state.safeAddress = accounts[9]
    state.tokensForSale = web3.utils.toWei('2000000') // fvt-wei
    state.initialPrice = web3.utils.toWei('0.0000020') // wei / fvt
    state.initialTrancheSize = web3.utils.toWei('500000') // fvt-wei
    state.initialDecay = web3.utils.toWei('0.0000001') // wei / fvt
    state.minPrice = web3.utils.toWei('0.0000001') // wei / fvt
    state.auction = await Auction.new(
      state.mgmt,
      state.token.address,
      state.uniswap.address,
      0,
      state.tokensForSale,
      state.initialPrice,
      state.initialTrancheSize,
      state.initialDecay,
      state.minPrice,
      state.safeAddress
    )
    await state.token.transfer(state.auction.address, state.tokensForSale, { from: state.mgmt })
  })

  afterEach(() => {})

  async function tranchePriceAtBlock (blocksExtra) {
    const currentTranche = await state.auction.currentTranche()
        // weiPerToken = wei / fvt
        // currentTokens = fvt-wei
        , tokenPrice = await computePriceForBlock(blocksExtra)
        , tranchePrice = tokenPrice.mul(new BN(currentTranche.currentTokens).div(decimals))
    return tranchePrice
  }

  async function computePriceForBlock (futureBlocks) {
    const currentTranche = await state.auction.currentTranche()
        , blockIssued = currentTranche.blockIssued.toNumber()
        , decayPerBlock = await state.auction.decayPerBlock()
        , tip = (await getChainHeight()).toNumber()
        , decay = decayPerBlock.mul(new BN(futureBlocks + tip - blockIssued))
        , weiPerToken = currentTranche.weiPerToken
        , minPrice = await state.auction.minimumPrice()
        , maybePrice = weiPerToken.sub(decay)
    if (maybePrice.lt(minPrice)) {
      return minPrice
    }
    return maybePrice
  }

  it('initializes values correctly', async function () {
    expect(await state.auction.management()).to.be.a('string').that.equals(state.mgmt)
    expect(await state.auction.token()).to.be.a('string').that.equals(state.token.address)
    expect(await state.auction.uniswap()).to.be.a('string').that.equals(state.uniswap.address)
    expect(await state.auction.startBlock()).to.eq.BN(await getChainHeight() - 1)
    expect(await state.auction.totalTokensOffered()).to.eq.BN(state.tokensForSale)
    expect(await state.auction.initialPrice()).to.eq.BN(state.initialPrice)
    expect(await state.auction.initialTrancheSize()).to.eq.BN(state.initialTrancheSize)
    expect(await state.auction.decayPerBlock()).to.eq.BN(state.initialDecay)
    expect(await state.auction.minimumPrice()).to.eq.BN(state.minPrice)
    expect(await state.auction.safeAddress()).to.be.a('string').that.equals(state.safeAddress)
  })

  it('allows purchasing tokens two ways', async function () {
    const purchaser = accounts[1]
        , balanceBefore = await state.token.balanceOf(purchaser)
        , toSpend = await tranchePriceAtBlock(1)
        , expectedPrice = await computePriceForBlock(1)
        , rcpt = await web3.eth.sendTransaction({ from: purchaser, to: state.auction.address, value: toSpend, gas: state.gasLimit })
        //        expectEvent(rcpt, 'PurchaseOccurred', {purchaser: purchaser, weiSpent: toSpend, weiReturned: 0, trancheNumber: 1})
        //        const currentPrice = await state.auction.getBuyPrice()
        , balanceAfter = await state.token.balanceOf(purchaser)
        // wei / (wei / token) = token
        , expectedTokens = web3.utils.toWei(toSpend).div(expectedPrice)
    expect(balanceBefore).to.eq.BN(0)
    expect(balanceAfter).to.eq.BN(expectedTokens)

    const purchaser2 = accounts[2]
        , balance2Before = await state.token.balanceOf(purchaser2)
        , rcpt2 = await state.auction.buy(expectedPrice.mul(new BN('2')), { from: purchaser2, value: toSpend, gas: state.gasLimit })
        , balance2After = await state.token.balanceOf(purchaser2)
        , currentPrice2 = await state.auction.getBuyPrice()
        , expectedTokens2 = web3.utils.toWei(toSpend).div(currentPrice2)
    expect(balance2Before).to.eq.BN(0)
    expect(balance2After).to.eq.BN(expectedTokens2)

    expectEvent(rcpt2, 'PurchaseOccurred', { purchaser: purchaser2, tokensAcquired: expectedTokens2, weiSpent: toSpend, weiReturned: '0', trancheNumber: '2' })
  })

  it('purchases do not extend past a lot', async function () {
    const purchaser = accounts[1]
        , balanceBefore = await state.token.balanceOf(purchaser)
        // fvt-wei * wei / fvt = fvt * 1e18 * wei / fvt = wei * 1e18
        , toSpend = await tranchePriceAtBlock(1)
        , rcpt = await state.auction.buy(state.initialPrice, { from: purchaser, value: toSpend.mul(new BN('2')), gas: state.gasLimit })
    expectEvent(rcpt, 'PurchaseOccurred', { purchaser: purchaser, tokensAcquired: state.initialTrancheSize, weiSpent: toSpend, weiReturned: toSpend, trancheNumber: '1' })
  })

  it('price decays linearly', async function () {
    const priceBefore = await state.auction.getBuyPrice()
        , decayRate = await state.auction.decayPerBlock()
    await mineBlocks(3)
    const priceAfter = await state.auction.getBuyPrice()
    expect(priceBefore.sub(priceAfter)).to.eq.BN(decayRate.mul(new BN('3')))
    await mineBlocks(5)
    const priceAfter2 = await state.auction.getBuyPrice()
    expect(priceAfter.sub(priceAfter2)).to.eq.BN(decayRate.mul(new BN('5')))
  })

  it('price decay rate doubles upon lot completion', async function () {
    const purchaser = accounts[1]
        , decayRateBefore = await state.auction.decayPerBlock()
        , toSpend = await tranchePriceAtBlock(1)
        , rcpt = await state.auction.buy(state.initialPrice, { from: purchaser, value: toSpend, gas: state.gasLimit })
    expectEvent(rcpt, 'PurchaseOccurred', { purchaser: purchaser, tokensAcquired: state.initialTrancheSize, weiSpent: toSpend, weiReturned: '0', trancheNumber: '1' })
    const decayRateAfter = await state.auction.decayPerBlock()
    expect(decayRateAfter.div(decayRateBefore)).to.eq.BN(2)
  })

  it('lot number increments upon lot completion', async function () {
    const purchaser = accounts[2]
        , purchaser2 = accounts[3]
        , lotNumberBefore = await state.auction.trancheNumber()
        , toSpend = await tranchePriceAtBlock(1)
        , rcpt = await state.auction.buy(state.initialPrice, { from: purchaser, value: toSpend, gas: state.gasLimit })
    expectEvent(rcpt, 'PurchaseOccurred', { purchaser: purchaser, tokensAcquired: state.initialTrancheSize, weiSpent: toSpend, weiReturned: '0', trancheNumber: '1' })
    const lotNumberAfter = await state.auction.trancheNumber()
        , toSpend2 = await tranchePriceAtBlock(1)
        , nextPrice = await computePriceForBlock(1)
        , rcpt2 = await state.auction.buy(nextPrice, { from: purchaser2, value: toSpend2, gas: state.gasLimit })
    expectEvent(rcpt2, 'PurchaseOccurred', { purchaser: purchaser2, tokensAcquired: new BN(state.initialTrancheSize).mul(new BN('2')), weiSpent: toSpend2, weiReturned: '0', trancheNumber: '2' })
    const lotNumberAfter2 = await state.auction.trancheNumber()

    expect(lotNumberBefore).to.eq.BN(1)
    expect(lotNumberAfter).to.eq.BN(2)
    expect(lotNumberAfter2).to.eq.BN(3)
  })

  it('prevents buying above maxPrice', async function () {
    const price = await computePriceForBlock(3)
    await expectRevert(state.auction.buy(price, { from: accounts[4], value: web3.utils.toWei('1'), gas: state.gasLimit }), 'price too high')
    const price2 = await computePriceForBlock(1)
    const rcpt = await state.auction.buy(price2, { from: accounts[4], value: web3.utils.toWei('1'), gas: state.gasLimit })
    expectEvent(rcpt, 'PurchaseOccurred')
  })

  it('prevents purchase before start block', async function () {
    const tip = await getChainHeight()
        , auction = await Auction.new(
          state.mgmt,
          state.token.address,
          state.uniswap.address,
          tip.add(new BN('5')),
          state.tokensForSale,
          state.initialPrice,
          state.initialTrancheSize,
          state.initialDecay,
          state.minPrice,
          state.safeAddress
        )
    await state.token.transfer(auction.address, state.tokensForSale, { from: accounts[0] })
    const price = web3.utils.toWei('1')
    await expectRevert(auction.buy(price, { from: accounts[4], value: web3.utils.toWei('1'), gas: state.gasLimit }), 'not started yet')
    await mineBlocks(11)
    const rcpt = await auction.buy(price, { from: accounts[4], value: web3.utils.toWei('1'), gas: state.gasLimit })
    expectEvent(rcpt, 'PurchaseOccurred', { purchaser: accounts[4] })
  })

  it('sells all tokens', async function () {
    for (var i = 0; i < 10; i++) {
      const price = await tranchePriceAtBlock(1)
      if (price.eq(new BN('0'))) {
        break
      }
      const rcpt = await state.auction.buy(web3.utils.toWei('1'), { from: accounts[i], value: price, gas: state.gasLimit })
    }
    const tokensSold = await state.auction.totalTokensSold()
    expect(tokensSold).to.eq.BN(state.tokensForSale)
  })

  it('allows management key to withdraw tokens', async function () {
    const balanceBefore = await state.token.balanceOf(state.mgmt)
    await expectRevert(state.auction.withdrawTokens({ from: accounts[3] }), 'Only management may call this')
    const balanceAfter = await state.token.balanceOf(state.mgmt)
        , auctionBalanceBefore = await state.token.balanceOf(state.auction.address)
    await state.auction.withdrawTokens({ from: state.mgmt })
    const balanceAfter2 = await state.token.balanceOf(state.mgmt)
        , auctionBalanceAfter = await state.token.balanceOf(state.auction.address)
    expect(balanceAfter).to.eq.BN(balanceBefore)
    expect(auctionBalanceAfter).to.eq.BN(0)
    expect(balanceAfter2.sub(balanceAfter)).to.eq.BN(auctionBalanceBefore)
  })

  it('only mgmt can change site hash', async function () {
    const notMgmt = accounts[2]
        , newSiteHash = '0x5555555555555555555555555555555555555555555555555555555555555555'
        , siteHashBefore = await state.auction.siteHash()
    await expectRevert(state.auction.setSiteHash(newSiteHash, { from: notMgmt }), 'Only management may call this')
    const siteHashAfter = await state.auction.siteHash()
    await state.auction.setSiteHash(newSiteHash, { from: state.mgmt })
    const siteHashAfter2 = await state.auction.siteHash()
    expect(siteHashBefore).to.be.a('string').that.equals(ZERO_BYTES32)
    expect(siteHashAfter).to.be.a('string').that.equals(ZERO_BYTES32)
    expect(siteHashAfter2).to.be.a('string').that.equals(newSiteHash)
  })

  it('only mgmt can pushLiquidity', async function () {
    const notMgmt = accounts[2]
    await expectRevert(state.auction.pushLiquidity({ from: notMgmt }), 'Only management may call this')
    const rcpt = await state.auction.pushLiquidity({ from: state.mgmt })
    expectEvent(rcpt, 'LiquidityPushed')
  })
   
  it('eventually hits price floor', async function () {
    for (var i = 0; i < 100; i++) {
      const rand = Math.random() * 10
          , expectedPrice = await computePriceForBlock(rand + 1)
      await mineBlocks(rand)
      const actualPrice = await state.auction.getBuyPrice()
      expect(actualPrice).to.eq.BN(expectedPrice)
    }

    const actualPrice = await state.auction.getBuyPrice()
    expect(actualPrice).to.eq.BN(state.minPrice)
  })

})
