'use strict';

const Web3 = require('web3');
const web3 = new Web3("ws://localhost:8545");
const bN = web3.utils.toBN;
const l = console.log;
const Identity = artifacts.require('Identity');
const Token = artifacts.require('Token');
const DummyERC721Receiver = artifacts.require('DummyERC721Receiver')
    , SafeMathLib = artifacts.require('SafeMathLib')

const {
  BN,           // Big Number support
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers');

const { ZERO_BYTES32, ZERO_ADDRESS } = constants; // empty hash


const chai = require('chai');
const bnChai = require('bn-chai');
chai.use(bnChai(BN));
chai.config.includeStack = true;
const expect = chai.expect;

async function mineOneBlock() {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_mine',
        id: new Date().getTime(),
    }, () => {});
}

async function mineBlocks(numBlocks) {
    for (var i = 0; i < numBlocks; i++) {
        await mineOneBlock();
    }
}


contract('Identity', function (accounts) {
    const state = {};
    const decimals = new BN('1000000000000000000')

    beforeEach(async () => {
        const safeMath = await SafeMathLib.new()
        await Token.link('SafeMathLib', safeMath.address)
        await Identity.link('SafeMathLib', safeMath.address)

        state.mgmt = accounts[0];
        state.initialBalance = new BN(1e9);
        state.token = await Token.new(state.mgmt);
        state.identity = await Identity.new(state.mgmt);
        await state.identity.setToken(state.token.address);
    });

    afterEach(() => {});

    it('only mgmt can change mgmt key', async function () {
        const notMgmt = accounts[2];
        const contractMgmtBefore = await state.identity.management();
        await expectRevert(state.identity.setManagement(notMgmt, {from: notMgmt}), 'Only management may call this')
        const contractMgmtAfter = await state.identity.management();
        expect(contractMgmtAfter).to.be.a('string').that.equals(contractMgmtBefore);
        const rcpt = await state.identity.setManagement(notMgmt, {from: contractMgmtBefore})
        expectEvent(rcpt, 'ManagementUpdated')
        const contractMgmtAfter2 = await state.identity.management();
        expect(contractMgmtAfter2).to.be.a('string').that.equals(notMgmt);
    })

    it('only mgmt can set token address', async function () {
        const token = await Token.new(state.mgmt);
        const identity = await Identity.new(state.mgmt);
        const notMgmt = accounts[2];
        const tokenBefore = await identity.token()
        expect(tokenBefore).to.be.a('string').that.equals(ZERO_ADDRESS);
        await expectRevert(identity.setToken(token.address, {from: notMgmt}), 'Only management may call this')
        const tokenAfter = await identity.token()
        expect(tokenAfter).to.be.a('string').that.equals(ZERO_ADDRESS);
        const rcpt = await identity.setToken(token.address, {from: state.mgmt})
        expectEvent(rcpt, 'TokenSet', {token: token.address})
        const tokenAfter2 = await identity.token()
        expect(tokenAfter2).to.be.a('string').that.equals(token.address);
    })

    it('only mgmt can mint identity for free', async function () {
        const notMgmt = accounts[2];
        const balanceBefore = await state.identity.balanceOf(notMgmt);
        await expectRevert(state.identity.createIdentityFor(notMgmt, {from: notMgmt}), 'Only management may call this')
        const balanceAfter = await state.identity.balanceOf(notMgmt);
        expect(balanceAfter).to.eq.BN(balanceBefore);
        const rcpt = await state.identity.createIdentityFor(notMgmt, {from: state.mgmt})
        expectEvent(rcpt, 'Transfer')
        expectEvent(rcpt, 'IdentityCreated')
        const balanceAfter2 = await state.identity.balanceOf(notMgmt);
        expect(balanceAfter2).to.eq.BN(1);
    })

    it('only mgmt can change identity increase factor', async function () {
        const notMgmt = accounts[2];
        const newIncreaseFactor = 3;
        const idIncreaseBefore = await state.identity.identityIncreaseFactor();
        expect(idIncreaseBefore).to.eq.BN(2);
        await expectRevert(state.identity.setIdentityIncreaseFactor(newIncreaseFactor, {from: notMgmt}), 'Only management may call this')
        const idIncreaseAfter = await state.identity.identityIncreaseFactor();
        expect(idIncreaseAfter).to.eq.BN(idIncreaseBefore);
        const rcpt = await state.identity.setIdentityIncreaseFactor(newIncreaseFactor, {from: state.mgmt})
        expectEvent(rcpt, 'IdentityIncreaseFactorUpdated')
        const idIncreaseAfter2 = await state.identity.identityIncreaseFactor();
        expect(idIncreaseAfter2).to.eq.BN(newIncreaseFactor);
    })

    it('only mgmt can change identity increase denominator', async function () {
        const notMgmt = accounts[2];
        const newIncreaseDenominator = 2;
        const idIncreaseBefore = await state.identity.identityIncreaseDenominator();
        expect(idIncreaseBefore).to.eq.BN(1);
        await expectRevert(state.identity.setIdentityIncreaseDenominator(newIncreaseDenominator, {from: notMgmt}), 'Only management may call this')
        const idIncreaseAfter = await state.identity.identityIncreaseDenominator();
        expect(idIncreaseAfter).to.eq.BN(idIncreaseBefore);
        const rcpt = await state.identity.setIdentityIncreaseDenominator(newIncreaseDenominator, {from: state.mgmt})
        expectEvent(rcpt, 'IdentityIncreaseDenominatorUpdated')
        const idIncreaseAfter2 = await state.identity.identityIncreaseDenominator();
        expect(idIncreaseAfter2).to.eq.BN(newIncreaseDenominator);
    })

    it('only mgmt can change identity decay factor', async function () {
        const notMgmt = accounts[2];
        const newDecayFactor = 2;

        const idDecayBefore = await state.identity.identityDecayFactor();
        expect(idDecayBefore).to.eq.BN(web3.utils.toWei('0.01'));
        await expectRevert(state.identity.setIdentityDecayFactor(newDecayFactor, {from: notMgmt}), 'Only management may call this')
        const idDecayAfter = await state.identity.identityDecayFactor();
        expect(idDecayAfter).to.eq.BN(idDecayBefore);

        const rcpt = await state.identity.setIdentityDecayFactor(newDecayFactor, {from: state.mgmt})
        expectEvent(rcpt, 'IdentityDecayFactorUpdated')
        const idDecayAfter2 = await state.identity.identityDecayFactor();
        expect(idDecayAfter2).to.eq.BN(newDecayFactor);
    })

    it('only mgmt can change identity price floor', async function () {
        const notMgmt = accounts[2];
        const newPriceFloor = 2;

        const priceFloorBefore = await state.identity.identityPriceFloor();
        expect(priceFloorBefore).to.eq.BN(web3.utils.toWei('100'));
        await expectRevert(state.identity.setIdentityPriceFloor(newPriceFloor, {from: notMgmt}), 'Only management may call this')
        const priceFloorAfter = await state.identity.identityPriceFloor();
        expect(priceFloorAfter).to.eq.BN(priceFloorBefore);

        const rcpt = await state.identity.setIdentityPriceFloor(newPriceFloor, {from: state.mgmt})
        expectEvent(rcpt, 'IdentityPriceFloorUpdated')
        const priceFloorAfter2 = await state.identity.identityPriceFloor();
        expect(priceFloorAfter2).to.eq.BN(newPriceFloor);
    })

    it('it manages the balances correctly', async function () {
        const balance1Before = await state.identity.balanceOf(accounts[0]);
        const balance2Before = await state.identity.balanceOf(accounts[1]);
        expect(balance1Before).to.eq.BN(0);
        expect(balance2Before).to.eq.BN(0);
        const rcpt = await state.identity.createIdentityFor(accounts[0], {from: state.mgmt})
        expectEvent(rcpt, 'Transfer')
        expectEvent(rcpt, 'IdentityCreated')
        const balance1After = await state.identity.balanceOf(accounts[0]);
        const balance2After = await state.identity.balanceOf(accounts[1]);
        expect(balance1After).to.eq.BN(1);
        expect(balance2After).to.eq.BN(0);
        const owner1Before = await state.identity.ownerOf(1);
        expect(owner1Before).to.be.a('string').that.equals(accounts[0]);
        await state.identity.createIdentityFor(accounts[1], {from: state.mgmt})
        const balance1After2 = await state.identity.balanceOf(accounts[0]);
        const balance2After2 = await state.identity.balanceOf(accounts[1]);
        expect(balance1After2).to.eq.BN(1);
        expect(balance2After2).to.eq.BN(1);
        const owner1After = await state.identity.ownerOf(1);
        const owner2After = await state.identity.ownerOf(2);
        expect(owner1After).to.be.a('string').that.equals(accounts[0]);
        expect(owner2After).to.be.a('string').that.equals(accounts[1]);
        const rcpt2 = await state.identity.transferFrom(accounts[0], accounts[1], 1, {from: accounts[0]});
        expectEvent(rcpt2, 'Transfer', {from: accounts[0], to: accounts[1], tokenId: new BN(1)})
        const balance1After3 = await state.identity.balanceOf(accounts[0]);
        const balance2After3 = await state.identity.balanceOf(accounts[1]);
        expect(balance1After3).to.eq.BN(0);
        expect(balance2After3).to.eq.BN(2);
        const owner1After2 = await state.identity.ownerOf(1);
        const owner2After2 = await state.identity.ownerOf(2);
        expect(owner1After2).to.be.a('string').that.equals(accounts[1]);
        expect(owner2After2).to.be.a('string').that.equals(accounts[1]);
        const rcpt3 = await state.identity.safeTransferFrom(accounts[1], accounts[2], 2, {from: accounts[1]});
        expectEvent(rcpt3, 'Transfer', {from: accounts[1], to: accounts[2], tokenId: new BN(2)})
        const balance1After4 = await state.identity.balanceOf(accounts[0]);
        const balance2After4 = await state.identity.balanceOf(accounts[1]);
        const balance3After4 = await state.identity.balanceOf(accounts[2]);
        expect(balance1After4).to.eq.BN(0);
        expect(balance2After4).to.eq.BN(1);
        expect(balance3After4).to.eq.BN(1);
        const owner1After3 = await state.identity.ownerOf(1);
        const owner2After3 = await state.identity.ownerOf(2);
        expect(owner1After3).to.be.a('string').that.equals(accounts[1]);
        expect(owner2After3).to.be.a('string').that.equals(accounts[2]);
    })

    it('accurately detects erc721 safe contracts', async function () {
        const dummy = await DummyERC721Receiver.new()
        await state.identity.createIdentityFor(accounts[0], {from: state.mgmt})
        const balance0Before = await state.identity.balanceOf(accounts[0]);
        const balanceTokenBefore = await state.identity.balanceOf(state.token.address);
        const balanceDummyBefore = await state.identity.balanceOf(dummy.address);
        const owner1Before = await state.identity.ownerOf(1);
        expect(balance0Before).to.eq.BN(1);
        expect(balanceTokenBefore).to.eq.BN(0);
        expect(balanceDummyBefore).to.eq.BN(0);
        expect(owner1Before).to.be.a('string').that.equals(accounts[0])

        // unspecified because of an openzeppelin bug, I think
        await expectRevert.unspecified(state.identity.safeTransferFrom(accounts[0], state.token.address, 1, {from: accounts[0]}));
        const balance0After = await state.identity.balanceOf(accounts[0]);
        const balanceTokenAfter = await state.identity.balanceOf(state.token.address);
        const balanceDummyAfter = await state.identity.balanceOf(dummy.address);
        const owner1After = await state.identity.ownerOf(1);
        expect(balance0After).to.eq.BN(1);
        expect(balanceTokenAfter).to.eq.BN(0);
        expect(balanceDummyAfter).to.eq.BN(0);
        expect(owner1After).to.be.a('string').that.equals(accounts[0])

        state.identity.safeTransferFrom(accounts[0], dummy.address, 1, {from: accounts[0]})
        const balance0After2 = await state.identity.balanceOf(accounts[0]);
        const balanceTokenAfter2 = await state.identity.balanceOf(state.token.address);
        const balanceDummyAfter2 = await state.identity.balanceOf(dummy.address);
        const owner1After2 = await state.identity.ownerOf(1);
        expect(balance0After2).to.eq.BN(0);
        expect(balanceTokenAfter2).to.eq.BN(0);
        expect(balanceDummyAfter2).to.eq.BN(1);
        expect(owner1After2).to.be.a('string').that.equals(dummy.address)
    })

    it('supports correct interfaces', async function () {
        const a = web3.utils.sha3('balanceOf(address)').slice(0,10);
        const b = web3.utils.sha3('ownerOf(uint256)').slice(0,10);
        const c = web3.utils.sha3('approve(address,uint256)').slice(0,10);
        const d = web3.utils.sha3('getApproved(uint256)').slice(0,10);
        const e = web3.utils.sha3('setApprovalForAll(address,bool)').slice(0,10);
        const f = web3.utils.sha3('isApprovedForAll(address,address)').slice(0,10);
        const g = web3.utils.sha3('transferFrom(address,address,uint256)').slice(0,10);
        const h = web3.utils.sha3('safeTransferFrom(address,address,uint256)').slice(0,10);
        const i = web3.utils.sha3('safeTransferFrom(address,address,uint256,bytes)').slice(0,10);
//        console.log(a, b, c, d, e, f, g, h, i)
        // xor the values, truncate to unsigned and print as hex string
        const combinedERC721 = '0x' + ((a ^ b ^ c ^ d ^ e ^ f ^ g ^ h ^ i) >>> 0).toString(16);
//        console.log('combined', combinedERC721)
        const supportsERC721 = await state.identity.supportsInterface(combinedERC721);
        expect(supportsERC721).to.be.true;
        const ERC165 = web3.utils.sha3('supportsInterface(bytes4)').slice(0,10);
        const supportsERC165 = await state.identity.supportsInterface(ERC165);
        expect(supportsERC165).to.be.true;
    })

    it('approve allows transfer by third party', async function () {
        const owner = accounts[0]
        const notOwner = accounts[1]
        const spender = accounts[2]
        await state.identity.createIdentityFor(owner, {from: state.mgmt})
        await state.identity.createIdentityFor(owner, {from: state.mgmt})
        await expectRevert(state.identity.transferFrom(owner, notOwner, 1, {from: spender}), 'Unapproved transfer')
        await expectRevert(state.identity.safeTransferFrom(owner, notOwner, 1, {from: spender}), 'Unapproved transfer')
        await expectRevert(state.identity.approve(spender, 1, {from: notOwner }), 'Not authorized to approve')
        const owner1Before = await state.identity.ownerOf(1);
        expect(owner1Before).to.be.a('string').that.equals(owner)
        await state.identity.approve(spender, 1, {from: owner})
        await state.identity.transferFrom(owner, notOwner, 1, {from: spender})
        const owner1After = await state.identity.ownerOf(1);
        expect(owner1After).to.be.a('string').that.equals(notOwner)
        await expectRevert(state.identity.safeTransferFrom(owner, notOwner, 2, {from: spender}), 'Unapproved transfer')
        await state.identity.approve(spender, 2, {from: owner})
        await state.identity.safeTransferFrom(owner, notOwner, 2, {from: spender})
        const owner2After = await state.identity.ownerOf(2);
        expect(owner2After).to.be.a('string').that.equals(notOwner)
    })

    it('setApprovalForAll allows spender to spend all', async function () {
        const owner = accounts[0]
        const notOwner = accounts[1]
        const spender = accounts[2]
        await state.identity.createIdentityFor(owner, {from: state.mgmt})
        await state.identity.createIdentityFor(owner, {from: state.mgmt})
        await expectRevert(state.identity.transferFrom(owner, notOwner, 1, {from: spender}), 'Unapproved transfer')
        await expectRevert(state.identity.transferFrom(owner, notOwner, 2, {from: spender}), 'Unapproved transfer')
        await expectRevert(state.identity.safeTransferFrom(owner, notOwner, 1, {from: spender}), 'Unapproved transfer')
        await expectRevert(state.identity.safeTransferFrom(owner, notOwner, 2, {from: spender}), 'Unapproved transfer')
        await state.identity.setApprovalForAll(spender, true, {from: owner})
        await state.identity.transferFrom(owner, notOwner, 1, {from: spender})
        await state.identity.transferFrom(owner, notOwner, 2, {from: spender})
        const owner1After = await state.identity.ownerOf(1);
        expect(owner1After).to.be.a('string').that.equals(notOwner)
        const owner2After = await state.identity.ownerOf(2);
        expect(owner2After).to.be.a('string').that.equals(notOwner)
    })

    it('requires tokens to be burned to mint own identity', async function () {
        const currentPrice = await state.identity.getIdentityPrice()
        await expectRevert(state.identity.createMyIdentity(0, {from: accounts[0]}), 'Underflow detected')
        await state.token.approve(state.identity.address, currentPrice, {from: accounts[0]})
        const balanceBefore = await state.token.balanceOf(accounts[0])
        const contractBalanceBefore = await state.token.balanceOf(state.identity.address)
        const tokenSupplyBefore = await state.token.totalSupply()
        const rcpt = await state.identity.createMyIdentity(0, {from: accounts[0]})
        expectEvent(rcpt, 'Transfer')
        expectEvent(rcpt, 'IdentityCreated')
        const balanceAfter = await state.token.balanceOf(accounts[0])
        const contractBalanceAfter = await state.token.balanceOf(state.identity.address)
        const tokenSupplyAfter = await state.token.totalSupply()
        expect(balanceBefore.sub(balanceAfter)).to.eq.BN(currentPrice)
        expect(contractBalanceBefore).to.eq.BN(0)
        expect(contractBalanceAfter).to.eq.BN(0)
        expect(tokenSupplyBefore.sub(tokenSupplyAfter)).to.eq.BN(currentPrice)
    })

    it('price increases according to parameters', async function () {
        const currentPriceBefore = await state.identity.getIdentityPrice()
        await state.token.approve(state.identity.address, currentPriceBefore, {from: accounts[0]})
        const rcpt = await state.identity.createMyIdentity(0, {from: accounts[0]})
        expectEvent(rcpt, 'Transfer')
        expectEvent(rcpt, 'IdentityCreated')
        const currentPriceAfter = await state.identity.getIdentityPrice()
        expect(currentPriceAfter.div(currentPriceBefore)).to.eq.BN(2)
    })

    it('price increase reacts to parameter changes', async function () {
        const currentPriceBefore = await state.identity.getIdentityPrice()
        await state.identity.setIdentityIncreaseFactor(8, {from: state.mgmt})
        await state.identity.setIdentityIncreaseDenominator(2, {from: state.mgmt})
        await state.token.approve(state.identity.address, currentPriceBefore, {from: accounts[0]})
        const rcpt = await state.identity.createMyIdentity(0, {from: accounts[0]})
        expectEvent(rcpt, 'Transfer')
        expectEvent(rcpt, 'IdentityCreated')
        const currentPriceAfter = await state.identity.getIdentityPrice()
        expect(currentPriceAfter.div(currentPriceBefore)).to.eq.BN(4)
    })

    it('price decreases linearly with time', async function () {
        const decayFactor = await state.identity.identityDecayFactor()
        const startPrice = await state.identity.getIdentityPrice()
        await state.token.approve(state.identity.address, startPrice, {from: accounts[0]})
        const rcpt = await state.identity.createMyIdentity(0, {from: accounts[0]})
        const currentPriceBefore = await state.identity.getIdentityPrice()
        await mineOneBlock()
        const currentPriceAfter = await state.identity.getIdentityPrice()
        await mineBlocks(2)
        const currentPriceAfter2 = await state.identity.getIdentityPrice()
        expect(currentPriceBefore.sub(currentPriceAfter)).to.eq.BN(decayFactor)
        expect(currentPriceBefore.sub(currentPriceAfter2)).to.eq.BN(decayFactor.mul(new BN(3)))
    })

    it('price decay responds to parameter changes', async function () {
        const expectedDecay = web3.utils.toWei('0.03')
        await state.identity.setIdentityDecayFactor(expectedDecay)
        const decayFactor = await state.identity.identityDecayFactor()
        expect(decayFactor).to.eq.BN(expectedDecay)
        const startPrice = await state.identity.getIdentityPrice()
        await state.token.approve(state.identity.address, startPrice, {from: accounts[0]})
        const rcpt = await state.identity.createMyIdentity(0, {from: accounts[0]})
        const currentPriceBefore = await state.identity.getIdentityPrice()
        await mineOneBlock()
        const currentPriceAfter = await state.identity.getIdentityPrice()
        await mineBlocks(2)
        const currentPriceAfter2 = await state.identity.getIdentityPrice()
        expect(currentPriceBefore.sub(currentPriceAfter)).to.eq.BN(decayFactor)
        expect(currentPriceBefore.sub(currentPriceAfter2)).to.eq.BN(decayFactor.mul(new BN(3)))
    })

})