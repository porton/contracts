/* global artifacts, web3, afterEach, beforeEach, it, contract */

const Web3 = require('web3')
const web3 = new Web3('ws://localhost:8545')
    , bN = web3.utils.toBN
    , l = console.log
    , LiquidityMining = artifacts.require('LiquidityMining')
    , Token = artifacts.require('Token')
    , SafeMathLib = artifacts.require('SafeMathLib')

const {
  BN, // Big Number support
  constants, // Common constants, like the zero address and largest integers
  expectEvent, // Assertions for emitted events
  expectRevert // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers')

const { ZERO_BYTES32, ZERO_ADDRESS } = constants // empty hash

const chai = require('chai')
    , bnChai = require('bn-chai')
chai.use(bnChai(BN))
chai.config.includeStack = true
const expect = chai.expect

async function mineOneBlock () {
  await web3.currentProvider.send({
    jsonrpc: '2.0',
    method: 'evm_mine',
    id: new Date().getTime()
  }, () => {})
}

async function mineBlocks (numBlocks) {
  for (var i = 0; i < numBlocks; i++) {
    await mineOneBlock()
  }
}

async function getChainHeight () {
  const tip = await web3.eth.getBlock('latest')
  return new BN(tip.number)
}

contract('Liquidity Mining', function (accounts) {
  const state = {}
      , decimals = new BN('1000000000000000000')

  beforeEach(async () => {
    const safeMath = await SafeMathLib.new()
    await Token.link('SafeMathLib', safeMath.address)
    await LiquidityMining.link('SafeMathLib', safeMath.address)
    state.mgmt = accounts[0]
    state.initialBalance = new BN(1e9)
    state.baseToken = await Token.new(state.mgmt)
    state.liquidityToken = await Token.new(state.mgmt)
    state.pulseWavelengthBlocks = new BN(10)
    state.pulseAmplitudeFVT = web3.utils.toWei('1000')
    state.maxStakers = new BN(5)
    state.liquidityMining = await LiquidityMining.new(
      state.baseToken.address,
      state.liquidityToken.address,
      state.mgmt,
      state.pulseWavelengthBlocks,
      state.pulseAmplitudeFVT,
      state.maxStakers
    )
    state.chainHeightStart = await getChainHeight()
    state.gasLimit = 170000
    state.safeAddress = accounts[9]
    state.tokenRewards = web3.utils.toWei('2000000') // fvt-wei

    await state.baseToken.transfer(state.liquidityMining.address, state.tokenRewards, { from: state.mgmt })
    for (let i = 1; i < 10; i++) {
      await state.liquidityToken.transfer(accounts[i], state.tokenRewards, { from: state.mgmt })
    }
  })

  afterEach(() => {})

  sumOfSquares = function (amplitude, wavelength) {
    const num = amplitude.mul(wavelength).mul(wavelength.add(new BN('1'))).mul((wavelength.mul(new BN('2'))).add(new BN('1')))
    const denom = wavelength.mul(wavelength).mul(new BN('6'))
    return num.div(denom)
  }

  it('only mgmt can change mgmt key', async function () {
      const notMgmt = accounts[2];
      const contractMgmtBefore = await state.liquidityMining.management();
      await expectRevert(state.liquidityMining.setManagement(notMgmt, {from: notMgmt}), 'Only management may call this')
      const contractMgmtAfter = await state.liquidityMining.management();
      expect(contractMgmtAfter).to.be.a('string').that.equals(contractMgmtBefore);
      const rcpt = await state.liquidityMining.setManagement(notMgmt, {from: contractMgmtBefore})
      expectEvent(rcpt, 'ManagementUpdated')
      const contractMgmtAfter2 = await state.liquidityMining.management();
      expect(contractMgmtAfter2).to.be.a('string').that.equals(notMgmt);
  })

  it('initializes values correctly', async function () {
    expect(await state.liquidityMining.management()).to.be.a('string').that.equals(state.mgmt)
    expect(await state.liquidityMining.baseToken()).to.be.a('string').that.equals(state.baseToken.address)
    expect(await state.liquidityMining.liquidityToken()).to.be.a('string').that.equals(state.liquidityToken.address)
    expect(await state.liquidityMining.pulseStartBlock()).to.eq.BN(state.chainHeightStart)
    expect(await state.liquidityMining.pulseWavelengthBlocks()).to.eq.BN(state.pulseWavelengthBlocks)
    expect(await state.liquidityMining.pulseAmplitudeFVT()).to.eq.BN(state.pulseAmplitudeFVT)
    expect(await state.liquidityMining.maxStakers()).to.eq.BN(state.maxStakers)
    expect(await state.baseToken.balanceOf(state.liquidityMining.address)).to.eq.BN(state.tokenRewards)
  })

  it('allows claiming and withdrawing from slots', async function  () {
    const slotBefore = await state.liquidityMining.slots(1)
    const deposit = web3.utils.toWei('100')
    const burnRate = web3.utils.toWei('1')
    await state.liquidityToken.approve(state.liquidityMining.address, deposit, {from: accounts[0]})
    const rcpt = await state.liquidityMining.claimSlot(1, burnRate, deposit, { from: accounts[0] })
    expectEvent(rcpt, 'SlotChangedHands', {slotId: '1', deposit: deposit, burnRate: burnRate, owner: accounts[0] })
    const slotAfter = await state.liquidityMining.slots(1)
    const heightAfter = await getChainHeight()
    const rewards = await state.liquidityMining.getRewards(1)
    const rcpt2 = await state.liquidityMining.withdrawFromSlot(1, { from: accounts[0] })
    const heightAfter2 = rcpt2.receipt.blockNumber;
    expectEvent(rcpt2, 'SlotChangedHands', {slotId: '1', deposit: '0', burnRate: '0', owner: ZERO_ADDRESS })
    const slotAfter2 = await state.liquidityMining.slots(1)

    expect(slotBefore.id).to.eq.BN(0)
    expect(slotBefore.lastUpdatedBlock).to.eq.BN(0)
    expect(slotBefore.deposit).to.eq.BN(0)
    expect(slotBefore.burnRate).to.eq.BN(0)
    expect(slotBefore.owner).to.be.a('string').that.equals(ZERO_ADDRESS)

    expect(slotAfter.id).to.eq.BN(1)
    expect(slotAfter.lastUpdatedBlock).to.eq.BN(heightAfter)
    expect(slotAfter.deposit).to.eq.BN(deposit)
    expect(slotAfter.burnRate).to.eq.BN(burnRate)
    expect(slotAfter.owner).to.be.a('string').that.equals(accounts[0])

    expect(slotAfter2.id).to.eq.BN(1)
    expect(slotAfter2.lastUpdatedBlock).to.eq.BN(heightAfter2)
    expect(slotAfter2.deposit).to.eq.BN(0)
    expect(slotAfter2.burnRate).to.eq.BN(0)
    expect(slotAfter2.owner).to.be.a('string').that.equals(ZERO_ADDRESS)

    await state.liquidityToken.approve(state.liquidityMining.address, deposit, {from: accounts[0]})
    await state.liquidityMining.claimSlot(2, burnRate, deposit, { from: accounts[0] })
    await expectRevert(state.liquidityMining.withdrawFromSlot(2, { from: accounts[1] }), 'Only owner can call this')
    console.log('amplitude', web3.utils.fromWei(await state.liquidityMining.pulseAmplitudeFVT()))
    console.log('integral', web3.utils.fromWei(await state.liquidityMining.pulseIntegral()))
    console.log('wavelength', (await state.liquidityMining.pulseWavelengthBlocks()).toString())
    const pulseWavelengthBlocks = await state.liquidityMining.pulseWavelengthBlocks()
//    let r2 = 0
//    for (let i = 0; i < pulseWavelengthBlocks * 6; i++) {
//      const r = web3.utils.fromWei(await state.liquidityMining.getRewards(1))
//      console.log(r, r - r2)
//      await mineOneBlock()
//      r2 = r
//    }

  })

  it('computes pulseIntegral correctly', async () => {
    const pulseIntegral = await state.liquidityMining.pulseIntegral()
    const pulseAmplitudeFVT = await state.liquidityMining.pulseAmplitudeFVT()
    const pulseWavelengthBlocks = await state.liquidityMining.pulseWavelengthBlocks()
    const deposit = web3.utils.toWei('100')
    const burnRate = web3.utils.toWei('1')
    await state.liquidityToken.approve(state.liquidityMining.address, deposit, {from: accounts[0]})
    await state.liquidityMining.claimSlot(1, burnRate, deposit, { from: accounts[0] })
    await mineBlocks(pulseWavelengthBlocks)
    const rewards = await state.liquidityMining.getRewards(1)
    expect(rewards).to.eq.BN(pulseIntegral)

    const numPulses = new BN(5);
    await mineBlocks(pulseWavelengthBlocks.mul(numPulses))
    const rewards2 = await state.liquidityMining.getRewards(1)
    expect(rewards2).to.eq.BN(pulseIntegral.mul(numPulses.add(new BN(1))))

    const sum = sumOfSquares(pulseAmplitudeFVT, pulseWavelengthBlocks)
    expect(pulseIntegral).to.eq.BN(sum)
  })

  it('allows management to update amplitude', async () => {
    const newAmplitude = new BN('2000')
    const notMgmt = accounts[1]
    const ampBefore = await state.liquidityMining.pulseAmplitudeFVT()
    await expectRevert(state.liquidityMining.setPulseAmplitude(newAmplitude, { from: notMgmt }), 'Only management may call this')
    const ampAfter = await state.liquidityMining.pulseAmplitudeFVT()
    const rcpt = await state.liquidityMining.setPulseAmplitude(newAmplitude, { from: state.mgmt })
    expectEvent(rcpt, 'AmplitudeUpdated')
    const ampAfter2 = await state.liquidityMining.pulseAmplitudeFVT()

    expect(ampBefore).to.eq.BN(ampAfter)
    expect(ampAfter2).to.eq.BN(newAmplitude)
    expect(ampAfter2).to.not.eq.BN(ampBefore)

    const pulseWavelengthBlocks = await state.liquidityMining.pulseWavelengthBlocks()
    const newIntegral = await state.liquidityMining.pulseIntegral()
    const sum = sumOfSquares(newAmplitude, pulseWavelengthBlocks)
    expect(newIntegral).to.eq.BN(sum)
  })

  it('allows management to update wavelength', async () => {
    const newWavelength = new BN('20')
    const notMgmt = accounts[1]
    const wvlBefore = await state.liquidityMining.pulseWavelengthBlocks()
    await expectRevert(state.liquidityMining.setPulseWavelength(newWavelength, { from: notMgmt }), 'Only management may call this')
    const wvlAfter = await state.liquidityMining.pulseWavelengthBlocks()
    const rcpt = await state.liquidityMining.setPulseWavelength(newWavelength, { from: state.mgmt })
    expectEvent(rcpt, 'WavelengthUpdated')
    const wvlAfter2 = await state.liquidityMining.pulseWavelengthBlocks()

    expect(wvlBefore).to.eq.BN(wvlAfter)
    expect(wvlAfter2).to.eq.BN(newWavelength)
    expect(wvlAfter2).to.not.eq.BN(wvlBefore)

    const pulseAmplitudeFVT = await state.liquidityMining.pulseAmplitudeFVT()
    const newIntegral = await state.liquidityMining.pulseIntegral()
    const sum = sumOfSquares(pulseAmplitudeFVT, newWavelength)
    expect(newIntegral).to.eq.BN(sum)
  })

  it('allows management to update maxStakers', async () => {
    const newMaxStakers = new BN('20')
    const notMgmt = accounts[1]
    const maxStakersBefore = await state.liquidityMining.maxStakers()
    await expectRevert(state.liquidityMining.setMaxStakers(newMaxStakers, { from: notMgmt }), 'Only management may call this')
    const maxStakersAfter = await state.liquidityMining.maxStakers()
    const rcpt = await state.liquidityMining.setMaxStakers(newMaxStakers, { from: state.mgmt })
    expectEvent(rcpt, 'MaxStakersUpdated')
    const maxStakersAfter2 = await state.liquidityMining.maxStakers()

    expect(maxStakersBefore).to.eq.BN(maxStakersAfter)
    expect(maxStakersAfter2).to.eq.BN(newMaxStakers)
    expect(maxStakersAfter2).to.not.eq.BN(maxStakersBefore)
  })

  it('allows management to update minimumDeposit', async () => {
    const newMinDeposit = new BN('20')
    const notMgmt = accounts[1]
    const minDepositBefore = await state.liquidityMining.minimumDeposit()
    await expectRevert(state.liquidityMining.setMinDeposit(newMinDeposit, { from: notMgmt }), 'Only management may call this')
    const minDepositAfter = await state.liquidityMining.minimumDeposit()
    const rcpt = await state.liquidityMining.setMinDeposit(newMinDeposit, { from: state.mgmt })
    expectEvent(rcpt, 'MinDepositUpdated')
    const minDepositAfter2 = await state.liquidityMining.minimumDeposit()

    expect(minDepositBefore).to.eq.BN(minDepositAfter)
    expect(minDepositAfter2).to.eq.BN(newMinDeposit)
    expect(minDepositAfter2).to.not.eq.BN(minDepositBefore)

    const deposit = new BN('10')
    const burnRate = web3.utils.toWei('10')
    await state.liquidityToken.approve(state.liquidityMining.address, deposit, {from: accounts[0]})
    await expectRevert(state.liquidityMining.claimSlot(1, burnRate, deposit, { from: accounts[0] }), 'Deposit must meet or exceed minimum')
  })

  it('allows management to update maximumDeposit', async () => {
    const newMaxDeposit = new BN('20')
    const notMgmt = accounts[1]
    const maxDepositBefore = await state.liquidityMining.maximumDeposit()
    await expectRevert(state.liquidityMining.setMaxDeposit(newMaxDeposit, { from: notMgmt }), 'Only management may call this')
    const maxDepositAfter = await state.liquidityMining.maximumDeposit()
    const rcpt = await state.liquidityMining.setMaxDeposit(newMaxDeposit, { from: state.mgmt })
    expectEvent(rcpt, 'MaxDepositUpdated')
    const maxDepositAfter2 = await state.liquidityMining.maximumDeposit()

    expect(maxDepositBefore).to.eq.BN(maxDepositAfter)
    expect(maxDepositAfter2).to.eq.BN(newMaxDeposit)
    expect(maxDepositAfter2).to.not.eq.BN(maxDepositBefore)

    const deposit = new BN('30')
    const burnRate = web3.utils.toWei('10')
    await state.liquidityToken.approve(state.liquidityMining.address, deposit, {from: accounts[0]})
    await expectRevert(state.liquidityMining.claimSlot(1, burnRate, deposit, { from: accounts[0] }), 'Deposit must not exceed maximum')
  })

  it('allows management to update minimumBurnRate', async () => {
    const newMinBurnRate = new BN('20')
    const notMgmt = accounts[1]
    const minBurnRateBefore = await state.liquidityMining.minimumBurnRate()
    await expectRevert(state.liquidityMining.setMinBurnRate(newMinBurnRate, { from: notMgmt }), 'Only management may call this')
    const minBurnRateAfter = await state.liquidityMining.minimumBurnRate()
    const rcpt = await state.liquidityMining.setMinBurnRate(newMinBurnRate, { from: state.mgmt })
    expectEvent(rcpt, 'MinBurnRateUpdated')
    const minBurnRateAfter2 = await state.liquidityMining.minimumBurnRate()

    expect(minBurnRateBefore).to.eq.BN(minBurnRateAfter)
    expect(minBurnRateAfter2).to.eq.BN(newMinBurnRate)
    expect(minBurnRateAfter2).to.not.eq.BN(minBurnRateBefore)

    const deposit = web3.utils.toWei('100')
    const burnRate = new BN('10')
    await state.liquidityToken.approve(state.liquidityMining.address, deposit, {from: accounts[0]})
    await expectRevert(state.liquidityMining.claimSlot(1, burnRate, deposit, { from: accounts[0] }), 'Burn rate must meet or exceed minimum')
  })

  it('deposit decays linearly with burn rate', async () => {
    const deposit = web3.utils.toWei('100')
    const burnRate = web3.utils.toWei('10')
    await state.liquidityToken.approve(state.liquidityMining.address, deposit, {from: accounts[0]})
    await state.liquidityMining.claimSlot(1, burnRate, deposit, { from: accounts[0] })
    const burn1 = await state.liquidityMining.getBurn(1)
    expect(burn1).to.eq.BN(0)
    let previousBurn = burn1;
    for (let i = 0; i < 10; i++) {
      await mineOneBlock()
      const burn = await state.liquidityMining.getBurn(1)
      expect(burn).to.eq.BN(previousBurn.add(new BN(burnRate)))
      previousBurn = burn;
    }
    await mineOneBlock()
    burn = await state.liquidityMining.getBurn(1)
    expect(burn).to.eq.BN(previousBurn)
    await mineOneBlock()
    burn = await state.liquidityMining.getBurn(1)
    expect(burn).to.eq.BN(previousBurn)
    await mineOneBlock()
    burn = await state.liquidityMining.getBurn(1)
    expect(burn).to.eq.BN(previousBurn)

    expect(burn).to.eq.BN(deposit)
  })

  it("allows buying out slots", async () => {
    const deposit = web3.utils.toWei('100')
    const lowerDeposit = web3.utils.toWei('10')
    const higherDeposit = web3.utils.toWei('101')

    const burnRate = web3.utils.toWei('10')
    const lowerBurnRate = web3.utils.toWei('1')
    const higherBurnRate = web3.utils.toWei('101')
    await state.liquidityToken.approve(state.liquidityMining.address, deposit, {from: accounts[0]})
    await state.liquidityMining.claimSlot(1, burnRate, deposit, { from: accounts[0] })

    await state.liquidityToken.approve(state.liquidityMining.address, higherDeposit, {from: accounts[1]})
    await expectRevert(state.liquidityMining.claimSlot(1, lowerBurnRate, higherDeposit, { from: accounts[1] }), 'You must outbid the current owner')
    await expectRevert(state.liquidityMining.claimSlot(1, higherBurnRate, lowerDeposit, { from: accounts[1] }), 'You must outbid the current owner')
    const rcpt = await state.liquidityMining.claimSlot(1, higherBurnRate, higherDeposit, { from: accounts[1] })
    expectEvent(rcpt, 'SlotChangedHands', {slotId: '1', deposit: higherDeposit, burnRate: higherBurnRate, owner: accounts[1]})
    // one block later deposit should be zero
    await mineOneBlock()
    const currentSlot = await state.liquidityMining.slots(1)
    const currentBurn = await state.liquidityMining.getBurn(1)
    expect(currentSlot.deposit).to.eq.BN(currentBurn)
    await state.liquidityToken.approve(state.liquidityMining.address, higherDeposit, {from: accounts[2]})
    const rcpt2 = await state.liquidityMining.claimSlot(1, lowerBurnRate, lowerDeposit, { from: accounts[2] })
    expectEvent(rcpt2, 'SlotChangedHands', {slotId: '1', deposit: lowerDeposit, burnRate: lowerBurnRate, owner: accounts[2]})
  })

  it("updateSlot pushes rewards and burns deposit", async () => {
    const deposit = web3.utils.toWei('100')
    const burnRate = web3.utils.toWei('10')
    await state.liquidityToken.approve(state.liquidityMining.address, deposit, {from: accounts[1]})
    await state.liquidityMining.claimSlot(1, burnRate, deposit, { from: accounts[1] })
    await mineBlocks(5)

    const burnBefore = await state.liquidityMining.getBurn(1)
    const rewardBefore = await state.liquidityMining.getRewards(1)
    const depositBefore = (await state.liquidityMining.slots(1)).deposit
    const fvtUserBalanceBefore = await state.baseToken.balanceOf(accounts[1])
    const fvtContractBalanceBefore = await state.baseToken.balanceOf(state.liquidityMining.address)
    const lpBalanceBefore = await state.liquidityToken.balanceOf(state.liquidityMining.address)

    await state.liquidityMining.updateSlot(1)

    const burnAfter = await state.liquidityMining.getBurn(1)
    const rewardAfter = await state.liquidityMining.getRewards(1)
    const depositAfter = (await state.liquidityMining.slots(1)).deposit
    const fvtUserBalanceAfter = await state.baseToken.balanceOf(accounts[1])
    const fvtContractBalanceAfter = await state.baseToken.balanceOf(state.liquidityMining.address)
    const lpBalanceAfter = await state.liquidityToken.balanceOf(state.liquidityMining.address)

    expect(burnAfter).to.eq.BN(0)
    expect(rewardAfter).to.eq.BN(0)
    expect(fvtUserBalanceBefore).to.eq.BN(0)
    expect(fvtUserBalanceAfter).to.eq.BN(fvtContractBalanceBefore.sub(fvtContractBalanceAfter))
    expect(lpBalanceBefore).to.eq.BN(deposit)
    expect(lpBalanceBefore.sub(lpBalanceAfter)).to.eq.BN(burnBefore.add(new BN(burnRate)))
    expect(burnBefore.add(new BN(burnRate))).to.eq.BN(depositBefore.sub(depositAfter))
  })

  it('prevents more stakers than maxStakers', async () => {
    const maxStakers = await state.liquidityMining.maxStakers()
    const newMaxStakers = new BN('10')

    const deposit = web3.utils.toWei('100')
    const burnRate = web3.utils.toWei('10')
    await state.liquidityToken.approve(state.liquidityMining.address, deposit, { from: accounts[1] })
    const rcpt = await state.liquidityMining.claimSlot(1, burnRate, deposit, { from: accounts[1] })

    await state.liquidityToken.approve(state.liquidityMining.address, deposit, { from: accounts[2] })
    await expectRevert(state.liquidityMining.claimSlot(10, burnRate, deposit, { from: accounts[2] }), 'Slot id out of range')
    await state.liquidityMining.setMaxStakers(newMaxStakers, {from: state.mgmt})
    await state.liquidityMining.claimSlot(10, burnRate, deposit, { from: accounts[2] })
    await state.liquidityMining.setMaxStakers('8', {from: state.mgmt})

    await state.liquidityToken.approve(state.liquidityMining.address, deposit, { from: accounts[3] })
    await expectRevert(state.liquidityMining.claimSlot(10, burnRate, deposit, { from: accounts[3] }), 'Slot id out of range')

    await state.liquidityMining.withdrawFromSlot(10, { from: accounts[2] })
  })

  it('allows random staking', async () => {
    let deposit = new BN(web3.utils.toWei('100'))
    let burnRate = new BN(web3.utils.toWei('10'))
    let numIters = 100;
    for (let i = 0;  i < 10; i++) {
      await state.liquidityToken.approve(state.liquidityMining.address, new BN(web3.utils.toWei('9999999999')), { from: accounts[i] })
    }

    while (numIters--) {
      let stakeIncrease = new BN(Math.floor(Math.random() * 10 + 1));
      let burnIncrease = new BN(Math.floor(Math.random() * 10 + 1));
      burnRate = burnRate.add(burnIncrease)
      deposit = deposit.add(stakeIncrease)
      let user = accounts[numIters % 10]
      let slotId = Math.floor(Math.random() * state.maxStakers + 1)
      const rcpt = await state.liquidityMining.claimSlot(slotId, burnRate, deposit, { from: user })
      expectEvent(rcpt, 'SlotChangedHands')
    }
  })

})
