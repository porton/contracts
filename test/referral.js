'use strict';

const Web3 = require('web3');
const web3 = new Web3("ws://localhost:8545");
const bN = web3.utils.toBN;
const l = console.log;
const Identity = artifacts.require('Identity');
const Token = artifacts.require('Token');
const Referral = artifacts.require('ReferralProgram')
    , SafeMathLib = artifacts.require('SafeMathLib')

const {
  BN,           // Big Number support
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers');

const { ZERO_BYTES32, ZERO_ADDRESS } = constants; // empty hash


const chai = require('chai');
const bnChai = require('bn-chai');
chai.use(bnChai(BN));
chai.config.includeStack = true;
const expect = chai.expect;

async function mineOneBlock() {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_mine',
        id: new Date().getTime(),
    }, () => {});
}

async function mineBlocks(numBlocks) {
    for (var i = 0; i < numBlocks; i++) {
        await mineOneBlock();
    }
}


contract('ReferralProgram', function (accounts) {
    const state = {};
    const decimals = new BN('1000000000000000000')

    beforeEach(async () => {
        const safeMath = await SafeMathLib.new()
        await Identity.link('SafeMathLib', safeMath.address)
        await Token.link('SafeMathLib', safeMath.address)

        state.mgmt = accounts[0];
        state.identity = await Identity.new(state.mgmt);
        state.token = await Token.new(state.mgmt);
        state.referral = await Referral.new(state.identity.address);
        await state.identity.setToken(state.token.address);
    });

    afterEach(() => {});

    it('records identity address correctly', async () => {
        const id = await state.referral.identityToken()
        expect(id).to.be.a('string').that.equals(state.identity.address)
    })

    it('allows referrals to be recorded by owner', async () => {
        await state.identity.createIdentityFor(accounts[0], {from: state.mgmt})
        await state.identity.createIdentityFor(accounts[1], {from: state.mgmt})
        const rcpt = await state.referral.setReferral(1, 2, {from: accounts[1]});
        expectEvent(rcpt, 'ReferralMade', {owner: accounts[1], referer: '1', referee: '2'})
        const referance = await state.referral.referralMap(2)
        expect(referance).to.eq.BN('1')
        const referance2 = await state.referral.referralMap(1)
        expect(referance2).to.eq.BN('0')
    })

    it('disallows referrals to be recorded by non-owners', async () => {
        await state.identity.createIdentityFor(accounts[0], {from: state.mgmt})
        await state.identity.createIdentityFor(accounts[1], {from: state.mgmt})
        await expectRevert(state.referral.setReferral(2, 1, {from: accounts[1]}), 'Cannot claim referral for token you do not own');
        const referance = await state.referral.referralMap(2)
        expect(referance).to.eq.BN('0')
        const referance2 = await state.referral.referralMap(1)
        expect(referance2).to.eq.BN('0')
    })
})
