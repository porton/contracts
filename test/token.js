'use strict';

const Web3 = require('web3');
const web3 = new Web3("ws://localhost:8545");
const bN = web3.utils.toBN;
const l = console.log;
const Token = artifacts.require('Token');
const SafeMathLib = artifacts.require('SafeMathLib');

const {
  BN,           // Big Number support
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers');


const chai = require('chai');
const bnChai = require('bn-chai');
chai.use(bnChai(BN));
chai.config.includeStack = true;
const expect = chai.expect;

async function assertEvents(promise, eventNames) {
    let rcpt = await promise;
    if (typeof (rcpt) === 'string') {
        rcpt = await web3.eth.getTransactionReceipt(rcpt);
    }

    assert(rcpt.logs.length === eventNames.length);
    for (let i = 0; i < rcpt.logs.length; i++) {
        assert(rcpt.logs[i].event === eventNames[i]);
    }
    return rcpt;
}

async function assertReverts(fxn, args) {
    try {
        await fxn(args);
        assert(false);
    } catch (e) {
        //
    }
}

async function increaseTime(bySeconds) {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_increaseTime',
        params: [bySeconds],
        id: new Date().getTime(),
    }, (err, result) => {
      if (err) { console.error(err) }

    });
    await mineOneBlock();
}

async function mineOneBlock() {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_mine',
        id: new Date().getTime(),
    }, () => {});
}

contract('Token', function (accounts) {
    const state = {};
    const decimals = new BN('1000000000000000000')

    beforeEach(async () => {
        const safeMath = await SafeMathLib.new()
        await Token.link('SafeMathLib', safeMath.address)
        state.centralBank = accounts[0];
        state.initialBalance = new BN(1e9);
        state.token = await Token.new(state.centralBank);
    });

    afterEach(() => {});

    it('it allows transfer of coins', async function () {
        const amountTransfer = decimals;
        const centralBankBalanceBefore = await state.token.balanceOf(state.centralBank);
        const receiverBalanceBefore = await state.token.balanceOf(accounts[1]);
        const totalSupplyBefore = await state.token.totalSupply();
        expect(centralBankBalanceBefore).to.eq.BN(state.initialBalance.mul(decimals));
        expect(receiverBalanceBefore).to.be.zero;
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance.mul(decimals));
        await assertEvents(state.token.transfer(accounts[1], amountTransfer), ['Transfer']);
        await assertReverts(state.token.transfer, [accounts[3], amountTransfer, {from: accounts[2]}]);
        const centralBankBalanceAfter = await state.token.balanceOf(state.centralBank);
        const receiverBalanceAfter = await state.token.balanceOf(accounts[1]);
        const totalSupplyAfter = await state.token.totalSupply();
        expect(centralBankBalanceBefore.sub(centralBankBalanceAfter)).to.eq.BN(amountTransfer);
        expect(receiverBalanceAfter).to.eq.BN(amountTransfer);
        expect(totalSupplyAfter).to.eq.BN(state.initialBalance.mul(decimals));
    });

    it('it allows transferFrom and manipulation of allowance', async () => {
        let owner = state.centralBank;
        let spender = accounts[1];
        let receiver = accounts[2];
        expect(owner !== spender);
        let initialAllowance = await state.token.allowance(owner, spender);
        expect(initialAllowance).to.be.zero;
        await assertReverts(state.token.transferFrom, [owner, receiver, bN(1), {from: spender}]);
        await assertReverts(state.token.approve, [spender, bN(1), {from: spender}]);
        await assertEvents(state.token.approve(spender, bN(1), {from: owner}), ['Approval']);
        let allowanceAfter = await state.token.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(1);
        await assertReverts(state.token.transferFrom, [owner, receiver, bN(2), {from: spender}]);
        allowanceAfter = await state.token.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(1);
        await assertEvents(state.token.transferFrom(owner, receiver, bN(1), {from: spender}), ['Transfer']);
        allowanceAfter = await state.token.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(0);
        await assertEvents(state.token.decreaseApproval(spender, bN(2), {from: owner}), ['Approval']);
        allowanceAfter = await state.token.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(0);
        await assertEvents(state.token.increaseApproval(spender, bN(2), {from: owner}), ['Approval']);
        allowanceAfter = await state.token.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(2);
        await assertEvents(state.token.increaseApproval(spender, bN(3), {from: owner}), ['Approval']);
        allowanceAfter = await state.token.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(5);
        await assertEvents(state.token.transferFrom(owner, receiver, bN(1), {from: spender}), ['Transfer']);
        allowanceAfter = await state.token.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(4);
        await assertEvents(state.token.decreaseApproval(spender, bN(2), {from: owner}), ['Approval']);
        allowanceAfter = await state.token.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(2);
        await assertEvents(state.token.transferFrom(owner, receiver, bN(2), {from: spender}), ['Transfer']);
        allowanceAfter = await state.token.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(0);
    });

    it('it allows freezing of coins', async function () {
        const amountTransfer = bN(1e18);
        const centralBankBalanceBefore = await state.token.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceBefore = await state.token.frozenBalanceOf(state.centralBank);
        const totalSupplyBefore = await state.token.totalSupply();
        const numCoinsFrozenBefore = await state.token.numCoinsFrozen();
        expect(centralBankBalanceBefore).to.eq.BN(state.initialBalance.mul(decimals));
        // expect(centralBankFrozenBalanceBefore).to.be.zero;
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance.mul(decimals));
        expect(numCoinsFrozenBefore).to.be.zero;
        await assertEvents(state.token.freeze(amountTransfer, 1), ['Transfer', 'TokensFrozen']);
        const centralBankBalanceAfter = await state.token.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceAfter = await state.token.frozenBalanceOf(state.centralBank);
        const totalSupplyAfter = await state.token.totalSupply();
        const numCoinsFrozenAfter = await state.token.numCoinsFrozen();
        const frozenStruct = await state.token.frozenTokensMap(bN(1));
        expect(centralBankBalanceBefore.sub(centralBankBalanceAfter)).to.eq.BN(amountTransfer);
        // expect(centralBankFrozenBalanceAfter).to.eq.BN(amountTransfer);
        expect(totalSupplyAfter).to.eq.BN(state.initialBalance.mul(decimals));
        expect(numCoinsFrozenAfter).to.eq.BN(amountTransfer);
        expect(frozenStruct.id).to.eq.BN(1);
        expect(frozenStruct.dateFrozen).to.not.be.zero;
        expect(frozenStruct.lengthFreezeDays).to.eq.BN(1);
        expect(frozenStruct.amount).to.eq.BN(amountTransfer);
        expect(frozenStruct.frozen).to.be.true;
        expect(frozenStruct.owner).to.be.a('string').that.equals(state.centralBank);
    });

    it('it disallows transfer of frozen coins', async function () {
        const amountTransfer = await state.token.totalSupply();
        const centralBankBalanceBefore = await state.token.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceBefore = await state.token.frozenBalanceOf(state.centralBank);
        const totalSupplyBefore = await state.token.totalSupply();
        const numCoinsFrozenBefore = await state.token.numCoinsFrozen();
        expect(centralBankBalanceBefore).to.eq.BN(state.initialBalance.mul(decimals));
        // expect(centralBankFrozenBalanceBefore).to.be.zero;
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance.mul(decimals));
        expect(numCoinsFrozenBefore).to.be.zero;
        await assertEvents(state.token.freeze(amountTransfer, 1), ['Transfer', 'TokensFrozen']);
        const centralBankBalanceAfter = await state.token.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceAfter = await state.token.frozenBalanceOf(state.centralBank);
        const totalSupplyAfter = await state.token.totalSupply();
        const numCoinsFrozenAfter = await state.token.numCoinsFrozen();
        const frozenStruct = await state.token.frozenTokensMap(bN(1));
        expect(centralBankBalanceBefore.sub(centralBankBalanceAfter)).to.eq.BN(amountTransfer);
        // expect(centralBankFrozenBalanceAfter).to.eq.BN(amountTransfer);
        expect(totalSupplyAfter).to.eq.BN(state.initialBalance.mul(decimals));
        expect(numCoinsFrozenAfter).to.eq.BN(amountTransfer);
        expect(frozenStruct.id).to.eq.BN(1);
        expect(frozenStruct.dateFrozen).to.not.be.zero;
        expect(frozenStruct.lengthFreezeDays).to.eq.BN(1);
        expect(frozenStruct.amount).to.eq.BN(amountTransfer);
        expect(frozenStruct.frozen).to.be.true;
        expect(frozenStruct.owner).to.be.a('string').that.equals(state.centralBank);
        assertReverts(state.token.transfer, [bN(1), {from: state.centralBank}]);
        assertReverts(state.token.transfer, [bN(1000000000000000), {from: state.centralBank}]);
    });

    it('it disallows only transfer of frozen coins, unfrozen transfer allowed', async function () {
        const amountTransfer = state.initialBalance.div(bN(2));
        const centralBankBalanceBefore = await state.token.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceBefore = await state.token.frozenBalanceOf(state.centralBank);
        const totalSupplyBefore = await state.token.totalSupply();
        const numCoinsFrozenBefore = await state.token.numCoinsFrozen();
        expect(centralBankBalanceBefore).to.eq.BN(state.initialBalance.mul(decimals));
        // expect(centralBankFrozenBalanceBefore).to.be.zero;
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance.mul(decimals));
        expect(numCoinsFrozenBefore).to.be.zero;
        await assertEvents(state.token.freeze(amountTransfer, 1), ['Transfer', 'TokensFrozen']);
        const centralBankBalanceAfter = await state.token.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceAfter = await state.token.frozenBalanceOf(state.centralBank);
        const totalSupplyAfter = await state.token.totalSupply();
        const numCoinsFrozenAfter = await state.token.numCoinsFrozen();
        const frozenStruct = await state.token.frozenTokensMap(bN(1));
        expect(centralBankBalanceBefore.sub(centralBankBalanceAfter)).to.eq.BN(amountTransfer);
        // expect(centralBankFrozenBalanceAfter).to.eq.BN(amountTransfer);
        expect(totalSupplyAfter).to.eq.BN(state.initialBalance.mul(decimals));
        expect(numCoinsFrozenAfter).to.eq.BN(amountTransfer);
        expect(frozenStruct.id).to.eq.BN(1);
        expect(frozenStruct.dateFrozen).to.not.be.zero;
        expect(frozenStruct.lengthFreezeDays).to.eq.BN(1);
        expect(frozenStruct.amount).to.eq.BN(amountTransfer);
        expect(frozenStruct.frozen).to.be.true;
        expect(frozenStruct.owner).to.be.a('string').that.equals(state.centralBank);
        await assertEvents(state.token.transfer(accounts[1], amountTransfer, {from: state.centralBank}), ['Transfer']);
        assertReverts(state.token.transfer, [bN(1), {from: state.centralBank}]);
        assertReverts(state.token.transfer, [bN(1000000000000000), {from: state.centralBank}]);
    });

    it('it allows unfreezing after timelimit', async function () {
        const amountTransfer = state.initialBalance.div(bN(2));
        const centralBankBalanceBefore = await state.token.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceBefore = await state.token.frozenBalanceOf(state.centralBank);
        const totalSupplyBefore = await state.token.totalSupply();
        const numCoinsFrozenBefore = await state.token.numCoinsFrozen();
        expect(centralBankBalanceBefore).to.eq.BN(state.initialBalance.mul(decimals));
        // expect(centralBankFrozenBalanceBefore).to.be.zero;
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance.mul(decimals));
        expect(numCoinsFrozenBefore).to.be.zero;
        await assertEvents(state.token.freeze(amountTransfer, 1, {from: state.centralBank}), ['Transfer', 'TokensFrozen']);
        const centralBankBalanceAfter = await state.token.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceAfter = await state.token.frozenBalanceOf(state.centralBank);
        const totalSupplyAfter = await state.token.totalSupply();
        const numCoinsFrozenAfter = await state.token.numCoinsFrozen();
        const frozenStructBefore = await state.token.frozenTokensMap(bN(1));
        expect(centralBankBalanceBefore.sub(centralBankBalanceAfter)).to.eq.BN(amountTransfer);
        // expect(centralBankFrozenBalanceAfter).to.eq.BN(amountTransfer);
        expect(totalSupplyAfter).to.eq.BN(state.initialBalance.mul(decimals));
        expect(numCoinsFrozenAfter).to.eq.BN(amountTransfer);
        expect(frozenStructBefore.id).to.eq.BN(1);
        expect(frozenStructBefore.dateFrozen).to.not.be.zero;
        expect(frozenStructBefore.lengthFreezeDays).to.eq.BN(1);
        expect(frozenStructBefore.amount).to.eq.BN(amountTransfer);
        expect(frozenStructBefore.frozen).to.be.true;
        expect(frozenStructBefore.owner).to.be.a('string').that.equals(state.centralBank);
        assertReverts(state.token.unFreeze, [bN(1), {from: state.centralBank}]);
        await increaseTime(60 * 60 * 24 + 1);
        await assertEvents(state.token.unFreeze(bN(1), {from: state.centralBank}), ['Transfer', 'TokensUnfrozen']);
        const frozenStructAfter = await state.token.frozenTokensMap(bN(1));
        expect(frozenStructAfter.id).to.eq.BN(1);
        expect(frozenStructAfter.dateFrozen).to.not.be.zero;
        expect(frozenStructAfter.lengthFreezeDays).to.eq.BN(1);
        expect(frozenStructAfter.amount).to.eq.BN(amountTransfer);
        expect(frozenStructAfter.frozen).to.be.false;
        expect(frozenStructAfter.owner).to.be.a('string').that.equals(state.centralBank);
    });

    it('it allows transfer after unfreezing', async function () {
        const amountTransfer = state.initialBalance;
        const centralBankBalanceBefore = await state.token.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceBefore = await state.token.frozenBalanceOf(state.centralBank);
        const totalSupplyBefore = await state.token.totalSupply();
        const numCoinsFrozenBefore = await state.token.numCoinsFrozen();
        expect(centralBankBalanceBefore).to.eq.BN(state.initialBalance.mul(decimals));
        // expect(centralBankFrozenBalanceBefore).to.be.zero;
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance.mul(decimals));
        expect(numCoinsFrozenBefore).to.be.zero;
        await assertEvents(state.token.freeze(amountTransfer, 1, {from: state.centralBank}), ['Transfer', 'TokensFrozen']);
        const centralBankBalanceAfter = await state.token.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceAfter = await state.token.frozenBalanceOf(state.centralBank);
        const totalSupplyAfter = await state.token.totalSupply();
        const numCoinsFrozenAfter = await state.token.numCoinsFrozen();
        const frozenStruct = await state.token.frozenTokensMap(bN(1));
        expect(centralBankBalanceBefore.sub(centralBankBalanceAfter)).to.eq.BN(amountTransfer);
        // expect(centralBankFrozenBalanceAfter).to.eq.BN(amountTransfer);
        expect(totalSupplyAfter).to.eq.BN(state.initialBalance.mul(decimals));
        expect(numCoinsFrozenAfter).to.eq.BN(amountTransfer);
        expect(frozenStruct.id).to.eq.BN(1);
        expect(frozenStruct.dateFrozen).to.not.be.zero;
        expect(frozenStruct.lengthFreezeDays).to.eq.BN(1);
        expect(frozenStruct.amount).to.eq.BN(amountTransfer);
        expect(frozenStruct.frozen).to.be.true;
        expect(frozenStruct.owner).to.be.a('string').that.equals(state.centralBank);
        await increaseTime(60 * 60 * 24 + 1);
        await assertEvents(state.token.unFreeze(bN(1), {from: state.centralBank}), ['Transfer', 'TokensUnfrozen']);
        await assertEvents(state.token.transfer(accounts[1], amountTransfer, {from: state.centralBank}), ['Transfer']);
    });

    it('it allows multiple simultaneous freezings', async () => {
        const freezeThese1 = bN(1e18);
        const freezeThese2 = bN(1e19);
        const freezeThese3 = bN(2e18);
        const numCoinsFrozenBefore = await state.token.numCoinsFrozen();
        const totalSupplyBefore = await state.token.totalSupply();
        await state.token.transfer(accounts[1], freezeThese2, {from:state.centralBank});
        await state.token.transfer(accounts[2], freezeThese3, {from:state.centralBank});
        const balance1Before = await state.token.balanceOf(state.centralBank);
        const balance2Before = await state.token.balanceOf(accounts[1]);
        const balance3Before = await state.token.balanceOf(accounts[2]);


        await assertEvents(state.token.freeze(freezeThese1, 1, {from: state.centralBank}), ['Transfer', 'TokensFrozen']);
        await increaseTime(60 * 60 * 24 + 1);
        const numCoinsFrozenAfter1 = await state.token.numCoinsFrozen();
        const totalSupplyAfter1 = await state.token.totalSupply();
        const balance1After1 = await state.token.balanceOf(state.centralBank);
        const balance2After1 = await state.token.balanceOf(accounts[1]);
        const balance3After1 = await state.token.balanceOf(accounts[2]);

        await assertEvents(state.token.freeze(freezeThese2, 1, {from: accounts[1]}), ['Transfer', 'TokensFrozen']);
        await increaseTime(60 * 60 * 24 + 1);
        const numCoinsFrozenAfter2 = await state.token.numCoinsFrozen();
        const totalSupplyAfter2 = await state.token.totalSupply();
        const balance1After2 = await state.token.balanceOf(state.centralBank);
        const balance2After2 = await state.token.balanceOf(accounts[1]);
        const balance3After2 = await state.token.balanceOf(accounts[2]);

        await assertEvents(state.token.freeze(freezeThese3, 1, {from: accounts[2]}), ['Transfer', 'TokensFrozen']);
        await increaseTime(60 * 60 * 24 + 1);
        const numCoinsFrozenAfter3 = await state.token.numCoinsFrozen();
        const totalSupplyAfter3 = await state.token.totalSupply();
        const balance1After3 = await state.token.balanceOf(state.centralBank);
        const balance2After3 = await state.token.balanceOf(accounts[1]);
        const balance3After3 = await state.token.balanceOf(accounts[2]);

        await assertEvents(state.token.unFreeze(bN(1), {from: accounts[0]}), ['Transfer', 'TokensUnfrozen']);
        await increaseTime(60 * 60 * 24 + 1);
        const numCoinsFrozenAfter4 = await state.token.numCoinsFrozen();
        const totalSupplyAfter4 = await state.token.totalSupply();
        const balance1After4 = await state.token.balanceOf(state.centralBank);
        const balance2After4 = await state.token.balanceOf(accounts[1]);
        const balance3After4 = await state.token.balanceOf(accounts[2]);

        await assertEvents(state.token.unFreeze(bN(3), {from: accounts[0]}), ['Transfer', 'TokensUnfrozen']);
        await increaseTime(60 * 60 * 24 + 1);
        const numCoinsFrozenAfter5 = await state.token.numCoinsFrozen();
        const totalSupplyAfter5 = await state.token.totalSupply();
        const balance1After5 = await state.token.balanceOf(state.centralBank);
        const balance2After5 = await state.token.balanceOf(accounts[1]);
        const balance3After5 = await state.token.balanceOf(accounts[2]);

        await assertEvents(state.token.unFreeze(bN(2), {from: accounts[0]}), ['Transfer', 'TokensUnfrozen']);
        await increaseTime(60 * 60 * 24 + 1);
        const numCoinsFrozenAfter6 = await state.token.numCoinsFrozen();
        const totalSupplyAfter6 = await state.token.totalSupply();

        const balance1After6 = await state.token.balanceOf(state.centralBank);
        const balance2After6 = await state.token.balanceOf(accounts[1]);
        const balance3After6 = await state.token.balanceOf(accounts[2]);

        expect(numCoinsFrozenBefore).to.eq.BN(0);
        expect(numCoinsFrozenAfter1).to.eq.BN(freezeThese1);
        expect(numCoinsFrozenAfter2).to.eq.BN(freezeThese1.add(freezeThese2));
        expect(numCoinsFrozenAfter3).to.eq.BN(freezeThese1.add(freezeThese2).add(freezeThese3));
        expect(numCoinsFrozenAfter4).to.eq.BN(freezeThese2.add(freezeThese3));
        expect(numCoinsFrozenAfter5).to.eq.BN(freezeThese2);
        expect(numCoinsFrozenAfter6).to.eq.BN(0);

        expect(totalSupplyAfter1).to.eq.BN(totalSupplyBefore);
        expect(totalSupplyAfter2).to.eq.BN(totalSupplyBefore);
        expect(totalSupplyAfter3).to.eq.BN(totalSupplyBefore);
        expect(totalSupplyAfter4).to.eq.BN(totalSupplyBefore);
        expect(totalSupplyAfter5).to.eq.BN(totalSupplyBefore);
        expect(totalSupplyAfter6).to.eq.BN(totalSupplyBefore);

        expect(balance1After1).to.eq.BN(balance1Before.sub(freezeThese1));
        expect(balance1After2).to.eq.BN(balance1Before.sub(freezeThese1));
        expect(balance1After3).to.eq.BN(balance1Before.sub(freezeThese1));
        expect(balance1After4).to.eq.BN(balance1Before);
        expect(balance1After5).to.eq.BN(balance1Before);
        expect(balance1After6).to.eq.BN(balance1Before);

        expect(balance2After1).to.eq.BN(balance2Before);
        expect(balance2After2).to.eq.BN(balance2Before.sub(freezeThese2));
        expect(balance2After3).to.eq.BN(balance2Before.sub(freezeThese2));
        expect(balance2After4).to.eq.BN(balance2Before.sub(freezeThese2));
        expect(balance2After5).to.eq.BN(balance2Before.sub(freezeThese2));
        expect(balance2After6).to.eq.BN(balance2Before);

        expect(balance3After1).to.eq.BN(balance3Before);
        expect(balance3After2).to.eq.BN(balance3Before);
        expect(balance3After3).to.eq.BN(balance3Before.sub(freezeThese3));
        expect(balance3After4).to.eq.BN(balance3Before.sub(freezeThese3));
        expect(balance3After5).to.eq.BN(balance3Before);
        expect(balance3After6).to.eq.BN(balance3Before);

    });

    it('it allows anyone to burn their tokens', async () => {
        let sender = state.centralBank;
        let burnThese = state.initialBalance.div(bN(3));
        let tooMany = state.initialBalance;
        let balanceBefore = await state.token.balanceOf(sender);
        let totalSupplyBefore = await state.token.totalSupply();
        await assertEvents(state.token.burn(burnThese, {from: sender}), ['Transfer', 'TokensBurned']);
        let balanceAfter1 = await state.token.balanceOf(sender);
        let totalSupplyAfter1 = await state.token.totalSupply();
        await assertReverts(state.token.burn, [tooMany, {from: sender}]);
        let balanceAfter2 = await state.token.balanceOf(sender);
        let totalSupplyAfter2 = await state.token.totalSupply();

        expect(balanceBefore).to.eq.BN(state.initialBalance.mul(decimals));
        expect(balanceAfter1).to.eq.BN(balanceBefore.sub(burnThese));
        expect(balanceAfter2).to.eq.BN(balanceAfter1);
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance.mul(decimals));
        expect(totalSupplyAfter1).to.eq.BN(totalSupplyBefore.sub(burnThese));
        expect(totalSupplyAfter2).to.eq.BN(totalSupplyAfter1);

        await state.token.transfer(accounts[1], burnThese, {from: state.centralBank});
        sender = accounts[1];
        balanceBefore = await state.token.balanceOf(sender);
        burnThese = balanceBefore.div(bN(3));
        tooMany = balanceBefore;
        totalSupplyBefore = await state.token.totalSupply();
        await assertEvents(state.token.burn(burnThese, {from: sender}), ['Transfer', 'TokensBurned']);
        balanceAfter1 = await state.token.balanceOf(sender);
        totalSupplyAfter1 = await state.token.totalSupply();
        await assertReverts(state.token.burn, [tooMany, {from: sender}]);
        balanceAfter2 = await state.token.balanceOf(sender);
        totalSupplyAfter2 = await state.token.totalSupply();

        expect(balanceAfter1).to.eq.BN(balanceBefore.sub(burnThese));
        expect(balanceAfter2).to.eq.BN(balanceAfter1);
        expect(totalSupplyAfter1).to.eq.BN(totalSupplyBefore.sub(burnThese));
        expect(totalSupplyAfter2).to.eq.BN(totalSupplyAfter1);

    });

    it('it allows central bank to mint tokens', async function () {
        const amountMint = bN('10000000000000000000');
        const centralBankBalanceBefore = await state.token.balanceOf(state.centralBank);
        const totalSupplyBefore = await state.token.totalSupply();
        expect(centralBankBalanceBefore).to.eq.BN(state.initialBalance.mul(decimals));
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance.mul(decimals));
        assertReverts(state.token.mint, [state.centralBank, amountMint, {from: accounts[1]}]);
        await assertEvents(state.token.mint(state.centralBank, amountMint, {from: state.centralBank}), ['Transfer',  'TokensMinted']);
        const centralBankBalanceAfter = await state.token.balanceOf(state.centralBank);
        const totalSupplyAfter = await state.token.totalSupply();
        expect(centralBankBalanceAfter).to.eq.BN(state.initialBalance.add(amountMint).mul(decimals));
        expect(totalSupplyAfter).to.eq.BN(state.initialBalance.add(amountMint).mul(decimals));
    });

    it('it allows central bank to reassign address', async function () {
        const notBank = accounts[1];
        const currentBank = await state.token.bank();
        expect(currentBank).to.be.a('string').that.equals(state.centralBank);
        assertReverts(state.token.setBank, [notBank, {from: notBank}])
        const newBank = await state.token.bank();
        expect(newBank).to.be.a('string').that.equals(state.centralBank);
        await assertEvents(state.token.setBank(notBank, {from: state.centralBank}), ['BankUpdated']);
        const newBank2 = await state.token.bank();
        expect(newBank2).to.be.a('string').that.equals(notBank);

    });

});