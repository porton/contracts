'use strict';

const Web3 = require('web3');
const web3 = new Web3("ws://localhost:8545");
const bN = web3.utils.toBN;
const l = console.log;
const Vault = artifacts.require('Vault');
const Token = artifacts.require('Token');
const SafeMathLib = artifacts.require('SafeMathLib');

const {
  BN,           // Big Number support
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers');


const chai = require('chai');
const bnChai = require('bn-chai');
chai.use(bnChai(BN));
chai.config.includeStack = true;
const expect = chai.expect;

async function assertEvents(promise, eventNames) {
    let rcpt = await promise;
    if (typeof (rcpt) === 'string') {
        rcpt = await web3.eth.getTransactionReceipt(rcpt);
    }

    assert(rcpt.logs.length === eventNames.length);
    for (let i = 0; i < rcpt.logs.length; i++) {
        assert(rcpt.logs[i].event === eventNames[i]);
    }
    return rcpt;
}

async function assertReverts(fxn, args) {
    try {
        await fxn(args);
        assert(false);
    } catch (e) {
        //
    }
}

async function increaseTime(bySeconds) {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_increaseTime',
        params: [bySeconds],
        id: new Date().getTime(),
    }, (err, result) => {
      if (err) { console.error(err) }

    });
    await mineOneBlock();
}

async function mineOneBlock() {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_mine',
        id: new Date().getTime(),
    }, () => {});
}

async function mineBlocks(numBlocks) {
    for (var i = 0; i < numBlocks; i++) {
        await mineOneBlock();
    }
}

async function getChainHeight() {
    const tip = await web3.eth.getBlock('latest')
    return new BN(tip.number);
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function almostEqual(a, b, tolerance) {
    l('almostEqual', a, b, tolerance)
    l('sub', a.sub(b))
    l('abs', a.sub(b).abs())
    expect(a.sub(b).abs()).to.be.lte.BN(tolerance);
}

contract('Vault', function (accounts) {
    const state = {};
    const decimals = new BN('1000000000000000000')

    async function expectedWithdrawalAmount(trancheId) {
        const tranche = await state.vault.tranches(trancheId);
        const vestingPeriodEnd = tranche[5];
        let withdrawalAmount;
        let chainHeight = (await getChainHeight()).add(new BN(1));
        if (chainHeight.gte(vestingPeriodEnd)) {
            withdrawalAmount = tranche[3];
        } else {
            const startBlock = tranche[7];
            const lastWithdrawalDate = tranche[6];
            const coinsPerBlock = tranche[2].div((tranche[5].sub(startBlock)))
            withdrawalAmount = chainHeight.sub(lastWithdrawalDate).mul(coinsPerBlock);
        }
        return withdrawalAmount;
    }

    async function checkWithdrawal(trancheId) {
        const trancheBefore = await state.vault.tranches(trancheId);
        const destination = trancheBefore[1];

        const currentCoinsBefore = trancheBefore[3];
        const destinationBalanceBefore = await state.token.balanceOf(destination);
        const vaultBalanceBefore = await state.token.balanceOf(state.vault.address);

        const withdrawalAmount = await expectedWithdrawalAmount(trancheId);
        await state.vault.withdraw(trancheId);

        const trancheAfter = await state.vault.tranches(trancheId);

        const currentCoinsAfter = trancheAfter[3];
        const destinationBalanceAfter = await state.token.balanceOf(destination);
        const vaultBalanceAfter = await state.token.balanceOf(state.vault.address);

        expect(vaultBalanceBefore.sub(vaultBalanceAfter)).to.eq.BN(withdrawalAmount);
        expect(destinationBalanceAfter.sub(destinationBalanceBefore)).to.eq.BN(withdrawalAmount);
        expect(currentCoinsBefore.sub(currentCoinsAfter)).to.eq.BN(withdrawalAmount);

        return withdrawalAmount
    }

    async function getTokensLeft(trancheId) {
        const tranche = await state.vault.tranches(trancheId)
        return tranche[3];
    }

    async function isTrancheLocked(trancheId) {
        const chainHeight = await getChainHeight()
        const tranche = await state.vault.tranches(trancheId)
        const lockEnd = tranche[4]
//        l('tranche', tranche, lockEnd.toString(), chainHeight.toString())
        return chainHeight.lte(lockEnd);
    }

    beforeEach(async () => {
        const safeMath = await SafeMathLib.new()
        await Token.link('SafeMathLib', safeMath.address)
        await Vault.link('SafeMathLib', safeMath.address)
        state.centralBank = accounts[0];
        state.initialBalance = new BN(1e9);
        state.token = await Token.new(state.centralBank);
        state.numTranches = 10;
        state.maxLockPeriod = 100;
        state.latestVestingEnd = new BN(0);
        state.latestLockPeriodEnd = new BN(0);

        const chainHeight = await getChainHeight();

        state.destinations = [];
        state.tokenAllocations = [];
        state.lockPeriodEnds = [];
        state.vestingPeriodEnds = [];
        state.startBlocks = [];

        for (var i = 0; i < state.numTranches; i++) {
            state.destinations.push(accounts[i]);

            const allocation = web3.utils.toWei(new BN((getRandomInt(10) + 1) * 1000000))
            state.tokenAllocations.push(allocation);

            const startBlock = chainHeight.add(new BN(getRandomInt(10)))
            state.startBlocks.push(startBlock);

            const lockEnd = (new BN(getRandomInt(state.maxLockPeriod) + (state.numTranches * 2))).add(startBlock);
            state.lockPeriodEnds.push(lockEnd);

            if (lockEnd.gt(state.latestLockPeriodEnd)) {
                state.latestLockPeriodEnd = lockEnd;
            }

            const vestingPeriodEnd = lockEnd.add(new BN(getRandomInt(10) + state.numTranches));
            state.vestingPeriodEnds.push(vestingPeriodEnd);
            if (vestingPeriodEnd.gt(state.latestVestingEnd)) {
                state.latestVestingEnd = vestingPeriodEnd;
            }
        }

//        l('state', state)

        state.vault = await Vault.new(
            state.token.address,
            state.destinations,
            state.tokenAllocations,
            state.lockPeriodEnds,
            state.vestingPeriodEnds,
            state.startBlocks,
            {gas: 3e6}
        );
        await state.token.transfer(state.vault.address, state.initialBalance.mul(decimals), {from: state.centralBank});
    });

    afterEach(() => {});

    it('it initializes correctly', async function () {
        for (var i = 0; i < state.numTranches; i++) {
            const trancheId = i + 1;
            const tranche = await state.vault.tranches(trancheId);
            expect(tranche[1]).to.be.a('string').that.equals(state.destinations[i]);
            expect(tranche[2]).to.eq.BN(state.tokenAllocations[i]);
            expect(tranche[3]).to.eq.BN(state.tokenAllocations[i]);
            expect(tranche[4]).to.eq.BN(state.lockPeriodEnds[i]);
            expect(tranche[5]).to.eq.BN(state.vestingPeriodEnds[i]);
            expect(tranche[6]).to.eq.BN(state.startBlocks[i]);
        }
    });

    it('it allows token holders to withdraw 100% of their funds after vesting', async function () {
        const chainHeight = await getChainHeight();
        const distance = state.latestVestingEnd.sub(chainHeight).add(new BN(10))
        await mineBlocks(distance);
        for (var i = 0; i < state.numTranches; i++) {
            const trancheId = i + 1;
            const withdrawal = await checkWithdrawal(trancheId);
            expect(withdrawal).to.eq.BN(state.tokenAllocations[i])
        }
    });

    it('it allows underfunding of vault', async function () {
        const statedAllocation = state.initialBalance.mul(decimals)
        const actualAllocation = statedAllocation.div(new BN(2));
        const destination = accounts[5]
        const token = await Token.new(state.centralBank);
        const vault = await Vault.new(
            token.address,
            [destination],
            [statedAllocation],
            state.lockPeriodEnds.slice(0,1),
            state.vestingPeriodEnds.slice(0,1),
            state.startBlocks.slice(0,1),
            {gas: 3e6}
        );
        await token.transfer(vault.address, actualAllocation, {from: state.centralBank});
        const chainHeight = await getChainHeight();
        const distance = state.vestingPeriodEnds[0].sub(chainHeight).add(new BN(10))
        await mineBlocks(distance);

        const destinationBalanceBefore = await token.balanceOf(destination);
        const vaultBalanceBefore = await token.balanceOf(vault.address);

        await vault.withdraw(1);

        const destinationBalanceAfter = await token.balanceOf(destination);
        const vaultBalanceAfter = await token.balanceOf(vault.address);

        expect(vaultBalanceAfter).to.eq.BN(0)
        expect(destinationBalanceBefore).to.eq.BN(0)
        expect(vaultBalanceBefore.sub(vaultBalanceAfter)).to.eq.BN(actualAllocation);
        expect(destinationBalanceAfter.sub(destinationBalanceBefore)).to.eq.BN(actualAllocation);

    })

    it('it disallows token holders to withdraw any funds before lock time expires', async function () {
        await mineBlocks(1);
        for (var i = 1; i <= state.numTranches; i++) {
            const tokens = await getTokensLeft(i)

            await assertReverts(state.vault.withdraw, [i])
            const tokens2 = await getTokensLeft(i)
            expect(tokens).to.eq.BN(tokens2);
        }

        const chainHeight = await getChainHeight();
        const distance = state.latestLockPeriodEnd.sub(chainHeight).add(new BN(10))
        await mineBlocks(distance);
        for (var i = 1; i <= state.numTranches; i++) {
            const tokens = await getTokensLeft(i)
            await state.vault.withdraw(i);
        }
    });

    it('it allows you to break up withdrawals into multiple', async function () {
        const chainHeight = await getChainHeight();
        const sums = {}
        for (var i = 1; i <= state.numTranches; i++) {
            sums[i] = new BN(0);
        }

        while ((await getChainHeight()).lte(state.latestVestingEnd.add(new BN(state.numTranches)))) {
            for (var i = 0; i < state.numTranches; i++) {
                const trancheId = i + 1;
                const isUnlocked = !await isTrancheLocked(trancheId);
                const coinsLeft = (await getTokensLeft(trancheId)).gt(new BN(0));
                if (isUnlocked && coinsLeft) {
                    sums[trancheId] = sums[trancheId].add(await checkWithdrawal(trancheId))
                }
                await mineBlocks(1);
            }
        }

        for (var i = 0; i < state.numTranches; i++) {
            const trancheId = i+1;
            expect(sums[trancheId]).to.eq.BN(state.tokenAllocations[i]);
        }
    });

    it('it allows forwarding of destinations', async function () {
      await mineBlocks(state.latestLockPeriodEnd)
      await expectRevert(state.vault.changeDestination(1, accounts[9], {from: accounts[9]}), 'Can only change destination if you are the destination')
      await expectRevert(state.vault.changeDestination(1, accounts[9], {from: accounts[8]}), 'Can only change destination if you are the destination')
      await expectRevert(state.vault.changeDestination(1, accounts[9], {from: accounts[7]}), 'Can only change destination if you are the destination')
      await expectRevert(state.vault.changeDestination(1, accounts[9], {from: accounts[6]}), 'Can only change destination if you are the destination')
      await expectRevert(state.vault.changeDestination(1, accounts[9], {from: accounts[5]}), 'Can only change destination if you are the destination')
      expect((await state.vault.tranches(1)).destination).to.be.a('string').that.equals(accounts[0])
      await state.vault.changeDestination(1, accounts[9], {from: accounts[0]})
      expect((await state.vault.tranches(1)).destination).to.be.a('string').that.equals(accounts[9])
      await checkWithdrawal(1)
    })

});