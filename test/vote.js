/* global artifacts, assert, contract */

const Web3 = require('web3')
const web3 = new Web3('ws://localhost:8545')
    , bN = web3.utils.toBN
    , l = console.log
    , Trollbox = artifacts.require('Trollbox')
    , Token = artifacts.require('Token')
    , Identity = artifacts.require('DummyERC721')
    , SafeMathLib = artifacts.require('SafeMathLib');

const {
  BN, // Big Number support
  constants, // Common constants, like the zero address and largest integers
  expectEvent, // Assertions for emitted events
  expectRevert // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers')

const { ZERO_BYTES32, ZERO_ADDRESS } = constants // empty hash

const chai = require('chai')
    , bnChai = require('bn-chai')
chai.use(bnChai(BN))
// chai.config.includeStack = true;
const expect = chai.expect

async function assertReverts (fxn, args) {
  try {
    await fxn(args)
    assert(false)
  } catch (e) {
    //
  }
}

async function increaseTime (bySeconds) {
  await web3.currentProvider.send({
    jsonrpc: '2.0',
    method: 'evm_increaseTime',
    params: [ bySeconds ],
    id: new Date().getTime()
  }, (err, result) => {
    if (err) { console.error(err) }
  })
  await mineOneBlock()
}

async function mineOneBlock () {
  await web3.currentProvider.send({
    jsonrpc: '2.0',
    method: 'evm_mine',
    id: new Date().getTime()
  }, () => {})
}

async function mineBlocks (numBlocks) {
  for (var i = 0; i < numBlocks; i++) {
    await mineOneBlock()
  }
}

async function getChainHeight () {
  const tip = await web3.eth.getBlock('latest')
  return new BN(tip.number)
}

async function getChainNow () {
  const tip = await web3.eth.getBlock('latest')
  return new BN(tip.timestamp)
}

contract('Trollbox', function (accounts) {
  const state = {}
      , decimals = new BN('1000000000000000000')

  async function createTournament () {
    const args = [
      state.defaultHash,
      state.defaultStartTime,
      state.defaultRoundLength,
      state.defaultBonus,
      state.defaultENS,
      state.defaultOracle,
      state.defaultMinRank,
      state.defaultUBI
    ]
    await state.trollbox.createTournament(...args, { from: state.mgmt })
  }

  beforeEach(async () => {
    const safeMath = await SafeMathLib.new()
    await Token.link('SafeMathLib', safeMath.address)
    await Identity.link('SafeMathLib', safeMath.address)
    await Trollbox.link('SafeMathLib', safeMath.address)
    state.mgmt = accounts[0]
    state.defaultOracle = accounts[1]
    state.rankManager = accounts[2]
    state.defaultHash = '0x2222222222222222222222222222222222222222222222222222222222222222'
    state.defaultENS = '0x1111111111111111111111111111111111111111111111111111111111111111'
    state.defaultRoundLength = new BN(500)
    state.initialBalance = new BN(1e9)
    state.defaultBonus = new BN(100)
    state.defaultMinRank = new BN(0)
    state.defaultUBI = new BN(100)
    state.defaultStartTime = (await getChainNow()).add(new BN(100))
    state.identity = await Identity.new()
    state.token = await Token.new(state.mgmt)
    state.trollbox = await Trollbox.new(state.mgmt, state.rankManager, state.identity.address)
    await state.trollbox.setToken(state.token.address)
    await state.token.transfer(state.trollbox.address, web3.utils.toWei(state.defaultBonus))
  })

  afterEach(() => {})

  it('only mgmt can change mgmt key', async function () {
    const notMgmt = accounts[2]
        , contractMgmtBefore = await state.trollbox.management()
    await assertReverts(state.trollbox.setManagement, [ notMgmt, { from: notMgmt } ])
    const contractMgmtAfter = await state.trollbox.management()
    expect(contractMgmtAfter).to.be.a('string').that.equals(contractMgmtBefore)
    await state.trollbox.setManagement(notMgmt, { from: contractMgmtBefore })
    const contractMgmtAfter2 = await state.trollbox.management()
    expect(contractMgmtAfter2).to.be.a('string').that.equals(notMgmt)
  })

  it('only mgmt can change rank manager', async function () {
    const notMgmt = accounts[2]
        , newRankManager = accounts[3]
        , rankManagerBefore = await state.trollbox.rankManager()
    await expectRevert(state.trollbox.setRankManager(newRankManager, { from: notMgmt }), 'Only management may call this')
    const rankManagerAfter = await state.trollbox.rankManager()
        , rcpt = await state.trollbox.setRankManager(newRankManager, { from: state.mgmt })
    expectEvent(rcpt, 'RankManagerUpdated', { oldManager: state.rankManager, newManager: newRankManager })
    const rankManagerAfter2 = await state.trollbox.rankManager()

    expect(rankManagerBefore).to.be.a('string').that.equals(state.rankManager)
    expect(rankManagerAfter).to.be.a('string').that.equals(state.rankManager)
    expect(rankManagerAfter2).to.be.a('string').that.equals(newRankManager)
  })

  it('only mgmt can set site hash', async function () {
    const notMgmt = accounts[2]
        , newSiteHash = '0x5555555555555555555555555555555555555555555555555555555555555555'
        , siteHashBefore = await state.trollbox.siteHash()
    await expectRevert(state.trollbox.setSiteHash(newSiteHash, { from: notMgmt }), 'Only management may call this')
    const siteHashAfter = await state.trollbox.siteHash()
        , rcpt = await state.trollbox.setSiteHash(newSiteHash, { from: state.mgmt })
    expectEvent(rcpt, 'SiteHashUpdated', { oldSiteHash: ZERO_BYTES32, newSiteHash: newSiteHash })
    const siteHashAfter2 = await state.trollbox.siteHash()

    expect(siteHashBefore).to.be.a('string').that.equals(ZERO_BYTES32)
    expect(siteHashAfter).to.be.a('string').that.equals(ZERO_BYTES32)
    expect(siteHashAfter2).to.be.a('string').that.equals(newSiteHash)
  })

  it('only mgmt can set token address', async function () {
    const notMgmt = accounts[2]
        , newTokenAddr = accounts[9]
        , tokenAddrBefore = await state.trollbox.token()
    await expectRevert(state.trollbox.setToken(newTokenAddr, { from: notMgmt }), 'Only management may call this')
    const tokenAddressAfter = await state.trollbox.token()
        , rcpt = await state.trollbox.setToken(newTokenAddr, { from: state.mgmt })
        , tokenAddressAfter2 = await state.trollbox.token()

    expect(tokenAddrBefore).to.be.a('string').that.equals(state.token.address)
    expect(tokenAddressAfter).to.be.a('string').that.equals(state.token.address)
    expect(tokenAddressAfter2).to.be.a('string').that.equals(newTokenAddr)
  })

  it('only rankManager can set rank', async function () {
    const notRankManager = accounts[3]
    expect(state.rankManager).to.not.equals(notRankManager)
    const newRank = '1'
        , voterId = '1'
        , idBefore = await state.trollbox.identities(voterId)
        , rankBefore = idBefore.rank
    await expectRevert(state.trollbox.setRank(voterId, newRank, { from: notRankManager }), 'Only rankManager may call this')
    const idAfter = await state.trollbox.identities(voterId)
        , rankAfter = idAfter.rank

        , rcpt = await state.trollbox.setRank(voterId, newRank, { from: state.rankManager })
    expectEvent(rcpt, 'RankUpdated', { voterId: voterId, oldRank: '0', newRank: newRank })
    const idAfter2 = await state.trollbox.identities(voterId)
        , rankAfter2 = idAfter2.rank

    expect(rankBefore).to.eq.BN(0)
    expect(rankAfter).to.eq.BN(0)
    expect(rankAfter2).to.eq.BN(newRank)
  })

  it('only mgmt can create a tournament', async function () {
    const notMgmt = accounts[2]
        , tournId = new BN(1)
        , contractTournBefore = await state.trollbox.tournaments(tournId)
    expect(contractTournBefore.tournamentId).to.eq.BN(0)
    const hash = '0x2222222222222222222222222222222222222222222222222222222222222222'
        , startTime = new BN(10)
        , roundLengthSeconds = new BN(100)
        , tokenRoundBonus = new BN(100)
        , tokenListENS = '0x1111111111111111111111111111111111111111111111111111111111111111'
        , oracle = accounts[9]
        , minRank = 0
        , voiceUBI = 100
        , args = [ hash, startTime, roundLengthSeconds, tokenRoundBonus, tokenListENS, oracle, minRank, voiceUBI ]
    await assertReverts(state.trollbox.createTournament, [ ...args, { from: notMgmt } ])
    const contractTournAfter = await state.trollbox.tournaments(tournId)
    expect(contractTournAfter.tournamentId).to.eq.BN(0)
    await state.trollbox.createTournament(...args, { from: state.mgmt })
    const contractTournAfter2 = await state.trollbox.tournaments(tournId)
    expect(contractTournAfter2.tournamentId).to.eq.BN(1)
    expect(contractTournAfter2.metadataHash).to.be.a('string').that.equals(hash)
    expect(contractTournAfter2.startTime).to.eq.BN(startTime)
    expect(contractTournAfter2.roundLengthSeconds).to.eq.BN(roundLengthSeconds)
    expect(contractTournAfter2.tokenRoundBonus).to.eq.BN(tokenRoundBonus)
    expect(contractTournAfter2.tokenListENS).to.be.a('string').that.equals(tokenListENS)
  })

  it('only mgmt can update a tournament', async function () {
    await createTournament()
    const notMgmt = accounts[2]
        , tournamentId = '1'
        , newMetadata = '0x3333333333333333333333333333333333333333333333333333333333333333'
        , newRoundBonus = new BN(200)
        , newMinRank = '1'
        , newUBI = new BN(200)
        , newTokenList = '0x4444444444444444444444444444444444444444444444444444444444444444'
        , newOracle = accounts[8]
        , args = [ tournamentId, newMetadata, newRoundBonus, newMinRank, newUBI, newTokenList, newOracle ]
        , tournBefore = await state.trollbox.tournaments(tournamentId)
    await expectRevert(state.trollbox.updateTournament(...args, { from: notMgmt }), 'Only management may call this')
    const tournAfter = await state.trollbox.tournaments(tournamentId)
        , rcpt = await state.trollbox.updateTournament(...args, { from: state.mgmt })
    expectEvent(rcpt, 'TournamentUpdated', {
      tournamentId,
      metadataHash: newMetadata,
      tokenRoundBonus: newRoundBonus,
      minimumRank: newMinRank,
      voiceUBI: newUBI,
      tokenListENS: newTokenList,
      winnerOracle: newOracle
    })
    const tournAfter2 = await state.trollbox.tournaments(tournamentId)

    expect(tournBefore.tournamentId).to.eq.BN(1)
    expect(tournBefore.metadataHash).to.be.a('string').that.equals(state.defaultHash)
    expect(tournBefore.startTime).to.eq.BN(state.defaultStartTime)
    expect(tournBefore.roundLengthSeconds).to.eq.BN(state.defaultRoundLength)
    expect(tournBefore.tokenRoundBonus).to.eq.BN(state.defaultBonus)
    expect(tournBefore.minimumRank).to.eq.BN(state.defaultMinRank)
    expect(tournBefore.voiceUBI).to.eq.BN(state.defaultUBI)
    expect(tournBefore.tokenListENS).to.be.a('string').that.equals(state.defaultENS)
    expect(tournBefore.winnerOracle).to.be.a('string').that.equals(state.defaultOracle)

    expect(tournAfter.tournamentId).to.eq.BN(1)
    expect(tournAfter.metadataHash).to.be.a('string').that.equals(state.defaultHash)
    expect(tournAfter.startTime).to.eq.BN(state.defaultStartTime)
    expect(tournAfter.roundLengthSeconds).to.eq.BN(state.defaultRoundLength)
    expect(tournAfter.tokenRoundBonus).to.eq.BN(state.defaultBonus)
    expect(tournAfter.minimumRank).to.eq.BN(state.defaultMinRank)
    expect(tournAfter.voiceUBI).to.eq.BN(state.defaultUBI)
    expect(tournAfter.tokenListENS).to.be.a('string').that.equals(state.defaultENS)
    expect(tournAfter.winnerOracle).to.be.a('string').that.equals(state.defaultOracle)

    expect(tournAfter2.tournamentId).to.eq.BN(1)
    expect(tournAfter2.metadataHash).to.be.a('string').that.equals(newMetadata)
    expect(tournAfter2.startTime).to.eq.BN(state.defaultStartTime)
    expect(tournAfter2.roundLengthSeconds).to.eq.BN(state.defaultRoundLength)
    expect(tournAfter2.tokenRoundBonus).to.eq.BN(newRoundBonus)
    expect(tournAfter2.minimumRank).to.eq.BN(newMinRank)
    expect(tournAfter2.voiceUBI).to.eq.BN(newUBI)
    expect(tournAfter2.tokenListENS).to.be.a('string').that.equals(newTokenList)
    expect(tournAfter2.winnerOracle).to.be.a('string').that.equals(newOracle)
  })

  it('only oracle is allowed to pick resolve rounds for tournament after 2 rounds', async function () {
    await createTournament()
    await increaseTime(101)
    const notOracle = accounts[5]
        , [ roundId1, winningOption1 ] = await state.trollbox.getRound(1, 1)
    expect(roundId1).to.eq.BN(0)
    expect(winningOption1).to.eq.BN(0)

    await assertReverts(state.trollbox.resolveRound, [ 1, 1, 1, { from: notOracle } ])
    const [ roundId2, winningOption2 ] = await state.trollbox.getRound(1, 1)
    expect(roundId2).to.eq.BN(0)
    expect(winningOption2).to.eq.BN(0)

    await assertReverts(state.trollbox.resolveRound, [ 1, 1, 1, { from: state.defaultOracle } ])
    const [ roundId3, winningOption3 ] = await state.trollbox.getRound(1, 1)
    expect(roundId3).to.eq.BN(0)
    expect(winningOption3).to.eq.BN(0)

    await increaseTime(state.defaultRoundLength.toNumber())
    await assertReverts(state.trollbox.resolveRound, [ 1, 1, 1, { from: notOracle } ])
    const [ roundId5, winningOption5 ] = await state.trollbox.getRound(1, 1)
    expect(roundId5).to.eq.BN(0)
    expect(winningOption5).to.eq.BN(0)

    await increaseTime(state.defaultRoundLength.toNumber())
    await state.trollbox.resolveRound(1, 1, 1, { from: state.defaultOracle })
    const [ roundId4, winningOption4 ] = await state.trollbox.getRound(1, 1)
    expect(roundId4).to.eq.BN(1)
    expect(winningOption4).to.eq.BN(1)
  })

  it('voting allowed only by identities', async function () {
    await createTournament()
    await increaseTime(state.defaultRoundLength.toNumber())
    const tournamentId = 1
        , voter = accounts[3]
        , voterId = 1
        , voteHash = '0x1111111111111111111111111111111111111111111111111111111111111111'
        , currentRoundId = await state.trollbox.getCurrentRoundId(tournamentId)
    await assertReverts(state.trollbox.vote, [ voterId, tournamentId, [ 1, 2, 3 ], [ 1, 2, 3 ], voteHash, { from: voter } ])
    await state.identity.createIdentityFor(voter, { from: state.mgmt })
    await state.trollbox.vote(voterId, tournamentId, [ 1, 2, 3 ], [ 1, 2, 3 ], voteHash, 0, { from: voter })
    const storedHash = await state.trollbox.getVoteMetadata(tournamentId, currentRoundId, voterId)
    expect(storedHash).to.be.a('string').that.equals(voteHash)
  })

  it('round ids progress correctly', async function () {
    await createTournament()
    const roundId1 = await state.trollbox.getCurrentRoundId(1)
    expect(roundId1).to.eq.BN(0)
    await increaseTime(state.defaultRoundLength.toNumber())
    const roundId2 = await state.trollbox.getCurrentRoundId(1)
    expect(roundId2).to.eq.BN(1)
    await increaseTime(state.defaultRoundLength.toNumber())
    const roundId3 = await state.trollbox.getCurrentRoundId(1)
    expect(roundId3).to.eq.BN(2)
    await increaseTime(state.defaultRoundLength.toNumber())
    const roundId4 = await state.trollbox.getCurrentRoundId(1)
    expect(roundId4).to.eq.BN(3)
  })

  it('voters get rewarded correctly', async function () {
    const tournamentId = new BN(1)
        , correctVoter = accounts[3]
        , correctVoterId = 1
        , incorrectVoter = accounts[4]
        , incorrectVoterId = 2
    await state.identity.createIdentityFor(correctVoter, { from: state.mgmt })
    await state.identity.createIdentityFor(incorrectVoter, { from: state.mgmt })
    await createTournament()
    await increaseTime(101)
    const roundId = await state.trollbox.getCurrentRoundId(tournamentId)
    await state.trollbox.vote(correctVoterId, tournamentId, [ 1 ], [ 10 ], ZERO_BYTES32, 0, { from: correctVoter })
    await state.trollbox.vote(incorrectVoterId, tournamentId, [ 2 ], [ 10 ], ZERO_BYTES32, 0, { from: incorrectVoter })

    const tokensWonBeforeCorrect = await state.trollbox.tokensWon(correctVoterId)
        , tokensWonBeforeIncorrect = await state.trollbox.tokensWon(incorrectVoterId)
    expect(tokensWonBeforeCorrect).to.eq.BN(0)
    expect(tokensWonBeforeIncorrect).to.eq.BN(0)

    await increaseTime(state.defaultRoundLength.toNumber())
    await increaseTime(state.defaultRoundLength.toNumber())

    await state.trollbox.resolveRound(tournamentId, roundId, 1, { from: state.defaultOracle })

    const lastRoundVotedCorrect = await state.trollbox.getLastRoundVoted(tournamentId, correctVoterId)
        , lastRoundVotedIncorrect = await state.trollbox.getLastRoundVoted(tournamentId, incorrectVoterId)
    expect(lastRoundVotedCorrect).to.eq.BN(1)
    expect(lastRoundVotedIncorrect).to.eq.BN(1)

    const correctNums = await state.trollbox.getRoundResults(correctVoterId, tournamentId, roundId)
        , incorrectNums = await state.trollbox.getRoundResults(incorrectVoterId, tournamentId, roundId)
    expect(correctNums['0']).to.eq.BN(10)
    expect(correctNums['1']).to.eq.BN(10)
    expect(incorrectNums['0']).to.eq.BN(0)
    expect(incorrectNums['1']).to.eq.BN(10)

    const lastRoundBonusCorrect = await state.trollbox.getRoundBonus(correctVoterId, tournamentId, roundId)
        , lastRoundBonusIncorrect = await state.trollbox.getRoundBonus(incorrectVoterId, tournamentId, roundId)

    expect(lastRoundBonusCorrect[0]).to.eq.BN(100)
    expect(lastRoundBonusCorrect[1]).to.eq.BN(100)
    expect(lastRoundBonusIncorrect[0]).to.eq.BN(0)
    expect(lastRoundBonusIncorrect[1]).to.eq.BN(0)

    await state.trollbox.updateAccount(correctVoterId, tournamentId, roundId)
    await state.trollbox.updateAccount(incorrectVoterId, tournamentId, roundId)
    const tokensWonAfterCorrect = await state.trollbox.tokensWon(correctVoterId)
        , tokensWonAfterIncorrect = await state.trollbox.tokensWon(incorrectVoterId)
    expect(tokensWonAfterCorrect).to.eq.BN(state.defaultBonus)
    expect(tokensWonAfterIncorrect).to.eq.BN(0)

    const fvtBalanceBeforeCorrect = await state.token.balanceOf(correctVoter)
        , fvtBalanceBeforeIncorrect = await state.token.balanceOf(incorrectVoter)
    expect(fvtBalanceBeforeCorrect).to.eq.BN(0)
    expect(fvtBalanceBeforeIncorrect).to.eq.BN(0)

    await state.trollbox.withdrawWinnings(correctVoterId)
    await expectRevert(state.trollbox.withdrawWinnings(incorrectVoterId), 'Nothing to withdraw')

    const fvtBalanceAfterCorrect = await state.token.balanceOf(correctVoter)
        , fvtBalanceAfterIncorrect = await state.token.balanceOf(incorrectVoter)

    expect(fvtBalanceAfterCorrect).to.eq.BN(state.defaultBonus)
    expect(fvtBalanceAfterIncorrect).to.eq.BN(0)

    const tokensWonAfter2Correct = await state.trollbox.tokensWon(correctVoterId)
        , tokensWonAfter2Incorrect = await state.trollbox.tokensWon(incorrectVoterId)
    expect(tokensWonAfter2Correct).to.eq.BN(0)
    expect(tokensWonAfter2Incorrect).to.eq.BN(0)
  })

  it('updateAccount is idempotent', async function () {
    const tournamentId = new BN(1)
        , voter = accounts[3]
        , voterId = 1
    await state.identity.createIdentityFor(voter, { from: state.mgmt })
    await createTournament()
    await increaseTime(101)
    const roundId = await state.trollbox.getCurrentRoundId(tournamentId)
    await state.trollbox.vote(voterId, tournamentId, [ 1 ], [ 10 ], ZERO_BYTES32, 0, { from: voter })
    await increaseTime(state.defaultRoundLength.toNumber() * 2)
    await state.trollbox.resolveRound(1, 1, 1, { from: state.defaultOracle })
    const voiceCreditsBefore = await state.trollbox.getVoiceCredits(tournamentId, voterId)
        , tokensWonBefore = await state.trollbox.tokensWon(voterId)
    await state.trollbox.updateAccount(voterId, tournamentId, roundId)
    const voiceCreditsAfter = await state.trollbox.getVoiceCredits(tournamentId, voterId)
        , tokensWonAfter = await state.trollbox.tokensWon(voterId)
    await state.trollbox.updateAccount(voterId, tournamentId, roundId)
    const voiceCreditsAfter2 = await state.trollbox.getVoiceCredits(tournamentId, voterId)
        , tokensWonAfter2 = await state.trollbox.tokensWon(voterId)

    expect(tokensWonBefore).to.eq.BN(0)
    expect(tokensWonAfter2).to.eq.BN(tokensWonAfter)
    expect(tokensWonAfter).to.eq.BN(state.defaultBonus)

    expect(voiceCreditsBefore).to.eq.BN(state.defaultUBI)
    expect(voiceCreditsAfter).to.eq.BN(state.defaultUBI.mul(new BN(2)))
    expect(voiceCreditsAfter2).to.eq.BN(voiceCreditsAfter)
  })

  it('voice credits accumulate over multiple wins', async function () {
    const tournamentId = new BN(1)
        , voter1 = accounts[3]
        , voterId1 = 1
        , voter2 = accounts[4]
        , voterId2 = 2
    await state.identity.createIdentityFor(voter1, { from: state.mgmt })
    await state.identity.createIdentityFor(voter2, { from: state.mgmt })
    await createTournament()

    await increaseTime(101)
    const roundId = await state.trollbox.getCurrentRoundId(tournamentId)
    await state.trollbox.vote(voterId1, tournamentId, [ 1 ], [ 10 ], ZERO_BYTES32, 0, { from: voter1 })
    await state.trollbox.vote(voterId2, tournamentId, [ 2 ], [ 10 ], ZERO_BYTES32, 0, { from: voter2 })

    await increaseTime(state.defaultRoundLength.toNumber())
    const roundId2 = await state.trollbox.getCurrentRoundId(tournamentId)
    await state.trollbox.vote(voterId1, tournamentId, [ 1, 2 ], [ 7, 7 ], ZERO_BYTES32, 0, { from: voter1 })
    await state.trollbox.vote(voterId2, tournamentId, [ 1, 2 ], [ 7, 7 ], ZERO_BYTES32, 0, { from: voter2 })

    await increaseTime(state.defaultRoundLength.toNumber())
    await state.trollbox.resolveRound(tournamentId, roundId, 1, { from: state.defaultOracle })

    const vcVoter1Before = await state.trollbox.getVoiceCredits(tournamentId, voterId1)
        , vcVoter2Before = await state.trollbox.getVoiceCredits(tournamentId, voterId2)
        , winningsVoter1Before = await state.trollbox.tokensWon(voterId1)
        , winningsVoter2Before = await state.trollbox.tokensWon(voterId2)

    await state.trollbox.updateAccount(voterId1, tournamentId, roundId)
    await state.trollbox.updateAccount(voterId2, tournamentId, roundId)

    const vcVoter1After = await state.trollbox.getVoiceCredits(tournamentId, voterId1)
        , vcVoter2After = await state.trollbox.getVoiceCredits(tournamentId, voterId2)
        , winningsVoter1After = await state.trollbox.tokensWon(voterId1)
        , winningsVoter2After = await state.trollbox.tokensWon(voterId2)

    await increaseTime(state.defaultRoundLength.toNumber())
    await state.trollbox.resolveRound(tournamentId, roundId2, 2, { from: state.defaultOracle })
    await state.trollbox.updateAccount(voterId1, tournamentId, roundId2)
    await state.trollbox.updateAccount(voterId2, tournamentId, roundId2)

    const vcVoter1After2 = await state.trollbox.getVoiceCredits(tournamentId, voterId1)
        , vcVoter2After2 = await state.trollbox.getVoiceCredits(tournamentId, voterId2)
        , winningsVoter1After2 = await state.trollbox.tokensWon(voterId1)
        , winningsVoter2After2 = await state.trollbox.tokensWon(voterId2)

    expect(vcVoter1Before).to.eq.BN(state.defaultUBI)
    expect(vcVoter2Before).to.eq.BN(state.defaultUBI)
    expect(winningsVoter1Before).to.eq.BN(0)
    expect(winningsVoter2Before).to.eq.BN(0)
    expect(vcVoter1After).to.eq.BN(state.defaultUBI.add(new BN(100)))
    expect(vcVoter2After).to.eq.BN(state.defaultUBI)
    expect(winningsVoter1After).to.eq.BN(state.defaultBonus)
    expect(winningsVoter2After).to.eq.BN(0)
    expect(vcVoter1After2).to.eq.BN(state.defaultUBI.add(new BN(100)).add(new BN(49)))
    expect(vcVoter2After2).to.eq.BN(state.defaultUBI.add(new BN(49)))
    expect(winningsVoter1After2).to.eq.BN(state.defaultBonus.add(state.defaultBonus.div(new BN(2))))
    expect(winningsVoter2After2).to.eq.BN(state.defaultBonus.div(new BN(2)))
  })

  it('allows updating accounts while voting', async () => {
    const tournamentId = new BN(1)
        , voter1 = accounts[3]
        , voterId1 = 1
    await state.identity.createIdentityFor(voter1, { from: state.mgmt })
    await createTournament()

    await increaseTime(101)
    const roundId = await state.trollbox.getCurrentRoundId(tournamentId)
    await state.trollbox.vote(voterId1, tournamentId, [ 1 ], [ 10 ], ZERO_BYTES32, 0, { from: voter1 })

    const vcBefore = await state.trollbox.getVoiceCredits(tournamentId, voterId1)
        , tokensBefore = await state.trollbox.tokensWon(voterId1)

    await increaseTime(state.defaultRoundLength.toNumber() * 2)
    await state.trollbox.resolveRound(tournamentId, roundId, 1, { from: state.defaultOracle })
    await state.trollbox.vote(voterId1, tournamentId, [ 1 ], [ 10 ], ZERO_BYTES32, roundId, { from: voter1 })

    const vcAfter = await state.trollbox.getVoiceCredits(tournamentId, voterId1)
        , tokensAfter = await state.trollbox.tokensWon(voterId1)

    expect(vcBefore).to.eq.BN(state.defaultUBI)
    expect(tokensBefore).to.eq.BN(0)
    expect(vcAfter).to.eq.BN(state.defaultUBI.add(new BN(100)))
    expect(tokensAfter).to.eq.BN(state.defaultBonus)
  })
})
